﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Speech;
using System.Speech.Synthesis;
using System.Speech.Recognition;
using System.Threading;
using latest_voicepad.Properties;

namespace latest_voicepad
{
    public partial class VoicepadForm : Form
    {
        #region notepadfields
        private bool isfilealreadysaved;
        private bool isfiledirty;
        private string currentopenfilename;
        private bool isfontsizeselected;
        private bool isfontnameselected;
        private bool colorbox;
        private bool textcolor;
        private bool dialogappear;
        private string pagecolor = "White";
        private Color fontcolor = Color.Black;
        private bool changingfont;
        private string choosenfont;
        private string nameoffont;
        private bool increasemore;
        private bool decreasemore;
        private bool pause;
        private bool find;
        private Label lblname;
        private string[] choices;
        private string[] words;
        private FontStyle bold;
        private FontStyle italic;
        private FontStyle underline;
        private FontStyle strikeout;
        private bool isboldchecked, isitalicchecked, isunderlinechecked, isstrikeoutchecked;
        private FontDialog fontdialog = new FontDialog();
        Stack<FontStyle> undofontstyle = new Stack<FontStyle>();
        Stack<FontStyle> redofontstyle = new Stack<FontStyle>();
        Stack<string> undoactions = new Stack<string>();
        Stack<string> redoactions = new Stack<string>();
        #endregion

        #region voicefields
        SpeechSynthesizer ss = new SpeechSynthesizer();   
        PromptBuilder pb = new PromptBuilder();
        SpeechRecognitionEngine sre = new SpeechRecognitionEngine();
        Choices clist = new Choices();
        #endregion

        public VoicepadForm()
        {
            InitializeComponent();
        }

        private void VoicepadForm_Load(object sender, EventArgs e)
        {
            #region notepadfields
            choices = new string[] { "abcd" };
            isfilealreadysaved = false;
            isfiledirty = false;
            currentopenfilename = "";

            if (Control.IsKeyLocked(Keys.CapsLock))
            {
                CapstoolStripStatusLabel.Text = "Caps ON";
            }
            else
                CapstoolStripStatusLabel.Text = "Caps OFF";
            #endregion                    
        }

        private void aboutVoicepadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("All right reserved with the Developer", "About!",
                             MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isfilealreadysaved == true)
            {
                if (isfiledirty == true)
                {
                    DialogResult ruslt = MessageBox.Show("Would you like to save your data?", "Ask!",
                                                          MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    if (ruslt == DialogResult.Yes)
                    {
                        if (Path.GetExtension(currentopenfilename) == ".txt")
                            mainrichTextBox.SaveFile(currentopenfilename, RichTextBoxStreamType.PlainText);

                        if (Path.GetExtension(currentopenfilename) == ".rtf")
                            mainrichTextBox.SaveFile(currentopenfilename, RichTextBoxStreamType.RichText);

                        isfiledirty = false;
                        SpeakAsyncSaveChecks();
                        NewToolStripMenu();
                        mainrichTextBox_Click(sender, e);
                    }

                    else if (ruslt == DialogResult.No)
                        NewToolStripMenu();
                        mainrichTextBox_Click(sender, e);
                }

                else
                    NewToolStripMenu();
                    mainrichTextBox_Click(sender, e);
            }

            else
            {
                if (isfiledirty == false)
                {
                    NewToolStripMenu();
                    mainrichTextBox_Click(sender, e);
                }

                else
                {
                    DialogResult rslt = MessageBox.Show("Would you like to save your data?", "Ask!",
                                                         MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    if (rslt == DialogResult.Yes)
                    {
                        SpeakAsyncSaveAsChecks();
                        SaveAsFileMenu();
                        if (isfilealreadysaved == true)
                        {
                            NewToolStripMenu();
                            mainrichTextBox_Click(sender, e);
                        }
                    }

                    else if (rslt == DialogResult.No)
                        NewToolStripMenu();
                        mainrichTextBox_Click(sender, e);
                }
            }
        }

        private void NewToolStripMenu()
        {
            if (disabledToolStripMenuItem.Enabled == true)
            { ss.SpeakAsync("and New document Created,"); }
            mainrichTextBox.Clear();
            isfilealreadysaved = false;
            isfiledirty = false;
            currentopenfilename = "";
            this.Text = "Untitled - VoicePad";
            mainrichTextBox.BackColor = Color.White;
            mainrichTextBox.SelectionFont = new Font(mainrichTextBox.Font, FontStyle.Regular);
            redoToolStripMenuItem.Enabled = false;
            undoToolStripMenuItem.Enabled = false;
            UndotoolStripButton.Enabled = false;
            RedotoolStripButton.Enabled = false;
            undoToolStripMenuItem1.Enabled = false;
            redoToolStripMenuItem1.Enabled = false;
            boldtoolStripMenuItem.Checked = false;
            BoldtoolStripButton.Checked = false;
            italicToolStripMenuItem.Checked = false;
            ItalictoolStripButton.Checked = false;
            underlineToolStripMenuItem.Checked = false;
            UnderlinetoolStripButton.Checked = false;
            strikethroughToolStripMenuItem.Checked = false;
            StrikethroughtoolStripButton.Checked = false;
            MessagetoolStripStatusLabel.Text = "New Document Has Created";
            undoactions.Clear();
            redoactions.Clear();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isfilealreadysaved == true)
            {
                if (isfiledirty == false)
                {
                    SpeakAsyncOpenChecks();
                    Openfiledialog();
                    mainrichTextBox_Click(sender, e);
                }

                else
                {
                    DialogResult ruslt = MessageBox.Show("Would you like to save your data?", "Ask!",
                                                          MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    if (ruslt == DialogResult.Yes)
                    {
                        if (Path.GetExtension(currentopenfilename) == ".txt")
                            mainrichTextBox.SaveFile(currentopenfilename, RichTextBoxStreamType.PlainText);

                        if (Path.GetExtension(currentopenfilename) == ".rtf")
                            mainrichTextBox.SaveFile(currentopenfilename, RichTextBoxStreamType.RichText);

                        isfiledirty = false;
                        SpeakAsyncSaveChecks();

                        SpeakAsyncOpenChecks();
                        Openfiledialog();
                        mainrichTextBox_Click(sender, e);
                    }

                    else if (ruslt == DialogResult.No)
                        SpeakAsyncOpenChecks();
                        Openfiledialog();
                        mainrichTextBox_Click(sender, e);
                }

            }

            else
            {
                if (isfiledirty == false)
                {
                    SpeakAsyncOpenChecks();
                    Openfiledialog();
                    mainrichTextBox_Click(sender, e);
                }

                else
                {
                    DialogResult ruslt = MessageBox.Show("Would you like to save your data?", "Ask!",
                                                          MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    if (ruslt == DialogResult.Yes)
                    {
                        SpeakAsyncSaveAsChecks();
                        SaveAsFileMenu();
                        if (isfilealreadysaved == true)
                        {
                            SpeakAsyncOpenChecks();
                            Openfiledialog();
                            mainrichTextBox_Click(sender, e);
                        }
                    }

                    else if (ruslt == DialogResult.No)
                        SpeakAsyncOpenChecks();
                        Openfiledialog();
                        mainrichTextBox_Click(sender, e);
                }
            }
        }

        private void Openfiledialog()
        {
            OpenFileDialog Opendialog = new OpenFileDialog();
            Opendialog.Filter = "Text Files (*.txt)|*.txt| Rich Text Format (*.rtf)|*.rtf";

            DialogResult result = Opendialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                if (Path.GetExtension(Opendialog.FileName) == ".txt")
                   mainrichTextBox.LoadFile(Opendialog.FileName, RichTextBoxStreamType.PlainText);

                if (Path.GetExtension(Opendialog.FileName) == ".rtf")
                    mainrichTextBox.LoadFile(Opendialog.FileName, RichTextBoxStreamType.RichText);

                this.Text = Path.GetFileName(Opendialog.FileName) + " - VoicePad";

                mainrichTextBox.BackColor = Color.White;
                isfilealreadysaved = true;
                isfiledirty = false;
                currentopenfilename = Opendialog.FileName;
                redoToolStripMenuItem.Enabled = false;
                undoToolStripMenuItem.Enabled = false;
                UndotoolStripButton.Enabled = false;
                RedotoolStripButton.Enabled = false;
                undoToolStripMenuItem1.Enabled = false;
                redoToolStripMenuItem1.Enabled = false;
                mainrichTextBox.SelectionFont = new Font(mainrichTextBox.Font, FontStyle.Regular);
                redoToolStripMenuItem.Enabled = false;
                undoToolStripMenuItem.Enabled = false;
                UndotoolStripButton.Enabled = false;
                RedotoolStripButton.Enabled = false;
                undoToolStripMenuItem1.Enabled = false;
                redoToolStripMenuItem1.Enabled = false;
                boldtoolStripMenuItem.Checked = false;
                BoldtoolStripButton.Checked = false;
                italicToolStripMenuItem.Checked = false;
                ItalictoolStripButton.Checked = false;
                underlineToolStripMenuItem.Checked = false;
                UnderlinetoolStripButton.Checked = false;
                strikethroughToolStripMenuItem.Checked = false;
                StrikethroughtoolStripButton.Checked = false;
                MessagetoolStripStatusLabel.Text = "File Has Opened";
                undoactions.Clear();
                redoactions.Clear();
                mainrichTextBox.SelectionStart = mainrichTextBox.Text.Length;
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isfilealreadysaved == true)
            {
                if (isfiledirty == true)
                {
                    if (Path.GetExtension(currentopenfilename) == ".txt")
                        mainrichTextBox.SaveFile(currentopenfilename, RichTextBoxStreamType.PlainText);

                    if (Path.GetExtension(currentopenfilename) == ".rtf")
                        mainrichTextBox.SaveFile(currentopenfilename, RichTextBoxStreamType.RichText);

                    SpeakAsyncSaveChecks();

                    isfiledirty = false;

                    MessagetoolStripStatusLabel.Text = "File Has Saved";

                }
                else
                    SpeakAsyncAlreadySaveChecks();
                MessagetoolStripStatusLabel.Text = "File Has Saved";
            }

            else
            {
                if (isfiledirty == false)
                {
                    DialogResult rlt = MessageBox.Show("Are you sure, You want to save an empty file?", "Ask!",
                                                         MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (rlt == DialogResult.Yes)
                        SpeakAsyncSaveAsChecks();
                    SaveAsFileMenu();
                }

                else
                    SpeakAsyncSaveAsChecks();
                SaveAsFileMenu();
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isfilealreadysaved == false)
            {
                if (isfiledirty == false)
                {
                    DialogResult rlt = MessageBox.Show("Are you sure, You want to save an empty file?", "Ask!",
                                                         MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (rlt == DialogResult.Yes)
                        SpeakAsyncSaveAsChecks();
                    SaveAsFileMenu();
                }

                else
                    SpeakAsyncSaveAsChecks();
                SaveAsFileMenu();
            }

            else
                SpeakAsyncSaveAsChecks();
            SaveAsFileMenu();
        }

        private void SaveAsFileMenu()
        {
            SaveFileDialog savedialog = new SaveFileDialog();
            savedialog.Filter = "Text Files (*.txt)|*.txt| Rich Text Format (*.rtf)|*.rtf";

            DialogResult result = savedialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                if (Path.GetExtension(savedialog.FileName) == ".txt")
                    mainrichTextBox.SaveFile(savedialog.FileName, RichTextBoxStreamType.PlainText);

                if (Path.GetExtension(savedialog.FileName) == ".rtf")
                    mainrichTextBox.SaveFile(savedialog.FileName, RichTextBoxStreamType.RichText);

                this.Text = Path.GetFileName(savedialog.FileName) + " - VoicePad";

                SpeakAsyncSaveChecks();

                isfilealreadysaved = true;
                isfiledirty = false;
                currentopenfilename = savedialog.FileName;
                MessagetoolStripStatusLabel.Text = "File Has Saved";
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void SpeakAsyncSaveAsChecks()
        {
            if (disabledToolStripMenuItem.Enabled == true)
                ss.SpeakAsync("opening save dialog, please save your file sir");
        }

        private void SpeakAsyncSaveChecks()
        {
            if (disabledToolStripMenuItem.Enabled == false)
            {
                MessageBox.Show("File Successfully Saved", "Save!", MessageBoxButtons.OK,
                               MessageBoxIcon.Information);
            }
            else
                ss.SpeakAsync("file successfully saved,");
        }

        private void SpeakAsyncOpenChecks()
        {
            if (disabledToolStripMenuItem.Enabled == true)
            {
                ss.SpeakAsync("opening file dialog, please select your file sir");
            }
        }

        private void SpeakAsyncAlreadySaveChecks()
        {
            if (disabledToolStripMenuItem.Enabled == true)
                ss.SpeakAsync("File Already Saved,");
            else
                MessageBox.Show("File Already Saved", "Save!", MessageBoxButtons.OK,
                                         MessageBoxIcon.Information);
        }

        private void mainrichTextBox_TextChanged(object sender, EventArgs e)
        {
            choices = null;
            choices = new string[] { "abcd" };
            choices = mainrichTextBox.Text.Split(' ');
            toolStripButton9.Enabled = true;
            isfiledirty = true;
            undoToolStripMenuItem.Enabled = true;
            UndotoolStripButton.Enabled = true;
            undoToolStripMenuItem1.Enabled = true;
            MessagetoolStripStatusLabel.Text = "File Does Not Save";
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (undoactions.Count < 1)
            {
                return;
            }

            if (redoactions.Count == 0 || mainrichTextBox.Text != "")
            {
                redofontstyle.Push(mainrichTextBox.SelectionFont.Style);
                redoactions.Push(mainrichTextBox.Text);
            }

            mainrichTextBox.SelectionFont = new Font (mainrichTextBox.Font, undofontstyle.Pop());
            mainrichTextBox.Text = undoactions.Pop();

            if (undoactions.Count == 0)
            {
                UndotoolStripButton.Enabled = false;
                undoToolStripMenuItem.Enabled = false;
                undoToolStripMenuItem1.Enabled = false;
            }

            mainrichTextBox.SelectionStart = mainrichTextBox.Text.Length;
            RedotoolStripButton.Enabled = true;
            redoToolStripMenuItem.Enabled = true;
            redoToolStripMenuItem1.Enabled = true;
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (redoactions.Count < 1)
            {
                return;
            }

            if (undoactions.Count == 0 || mainrichTextBox.Text != "")
            {
                undofontstyle.Push(mainrichTextBox.SelectionFont.Style);
                undoactions.Push(mainrichTextBox.Text);
            }

            mainrichTextBox.SelectionFont = new Font(mainrichTextBox.Font, redofontstyle.Pop());
            mainrichTextBox.Text = redoactions.Pop();

            if (redoactions.Count == 0)
            {
                RedotoolStripButton.Enabled = false;
                redoToolStripMenuItem.Enabled = false;
                redoToolStripMenuItem1.Enabled = false;
            }

            mainrichTextBox.SelectionStart = mainrichTextBox.Text.Length;
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            undoactions.Push(mainrichTextBox.Text);
            mainrichTextBox.SelectAll();
            mainrichTextBox_Click(sender, e);
        }

        private void dateTimeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainrichTextBox.SelectedText = DateTime.Now.ToString();
        }

        private void italicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (italicToolStripMenuItem.Checked == true)
            {
                ItalictoolStripButton.Checked = true;
                ItalictoolStripButton_Click(sender, e);
            }
            else
                ItalictoolStripButton.Checked = false;
            ItalictoolStripButton_Click(sender, e);
        }

        private void underlineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (underlineToolStripMenuItem.Checked == true)
            {
                UnderlinetoolStripButton.Checked = true;
                UnderlinetoolStripButton_Click(sender, e);
            }
            else
                UnderlinetoolStripButton.Checked = false;
            UnderlinetoolStripButton_Click(sender, e);
        }

        private void boldtoolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (boldtoolStripMenuItem.Checked == true)
            {
                BoldtoolStripButton.Checked = true;
                BoldtoolStripButton_Click(sender, e);
            }
            else
                BoldtoolStripButton.Checked = false;
            BoldtoolStripButton_Click(sender, e);
        }

        private void strikethroughToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (strikethroughToolStripMenuItem.Checked == true)
            {
                StrikethroughtoolStripButton.Checked = true;
                StrikethroughtoolStripButton_Click(sender, e);
            }
            else
                StrikethroughtoolStripButton.Checked = false;
            StrikethroughtoolStripButton_Click(sender, e);
        }

        private void normalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            undoactions.Push(mainrichTextBox.Text);
            mainrichTextBox.SelectionFont = new Font(mainrichTextBox.Font, FontStyle.Regular);
            boldtoolStripMenuItem.Checked = false;
            BoldtoolStripButton.Checked = false;
            italicToolStripMenuItem.Checked = false;
            ItalictoolStripButton.Checked = false;
            underlineToolStripMenuItem.Checked = false;
            UnderlinetoolStripButton.Checked = false;
            strikethroughToolStripMenuItem.Checked = false;
            StrikethroughtoolStripButton.Checked = false;
        }

        private void formatFontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fontdialog.ShowColor = true;
            fontdialog.ShowApply = true;

            DialogResult result = fontdialog.ShowDialog();

            fontdialog.Apply += new System.EventHandler(fontdialog_Apply);

            if (result == DialogResult.OK)
            {
                if (mainrichTextBox.SelectionLength > 0)
                {
                    mainrichTextBox.SelectionFont = fontdialog.Font;
                    mainrichTextBox.SelectionColor = fontdialog.Color;
                }
            }
        }

        private void fontdialog_Apply(object sender, EventArgs e)
        {
            if (mainrichTextBox.SelectionLength > 0)
            {
                mainrichTextBox.SelectionFont = fontdialog.Font;
                mainrichTextBox.SelectionColor = fontdialog.Color;
            }
        }

        private void changeTextColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog colordialog = new ColorDialog();

            DialogResult result = colordialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                if (mainrichTextBox.SelectionLength > 0)
                    mainrichTextBox.SelectionColor = colordialog.Color;
            }
        }

        private void NewtoolStripButton_Click(object sender, EventArgs e)
        {
            newToolStripMenuItem_Click(sender, e);
        }

        private void OpentoolStripButton_Click(object sender, EventArgs e)
        {
            openToolStripMenuItem_Click(sender, e);
        }

        private void SavetoolStripButton_Click(object sender, EventArgs e)
        {
            saveToolStripMenuItem_Click(sender, e);
        }

        private void SaveAstoolStripButton_Click(object sender, EventArgs e)
        {
            saveAsToolStripMenuItem_Click(sender, e);
        }

        private void PrinttoolStripButton_Click(object sender, EventArgs e)
        {

        }
              
        private void PrintPreviewtoolStripButton_Click(object sender, EventArgs e)
        {

        }

        private void UndotoolStripButton_Click(object sender, EventArgs e)
        {
            undoToolStripMenuItem_Click(sender, e);
        }

        private void RedotoolStripButton_Click(object sender, EventArgs e)
        {
            redoToolStripMenuItem_Click(sender, e);
        }

        private void SelectAlltoolStripButton_Click(object sender, EventArgs e)
        {
            selectAllToolStripMenuItem_Click(sender, e);
        }

        private void BoldtoolStripButton_Click(object sender, EventArgs e)
        {        
            if (BoldtoolStripButton.Checked == true)
            {
                undofontstyle.Push(mainrichTextBox.SelectionFont.Style);
                undoactions.Push(mainrichTextBox.Text);            
                mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), mainrichTextBox.SelectionFont.Style | FontStyle.Bold);
                boldtoolStripMenuItem.Checked = true;
                isboldchecked = true;
            }
            else
            {
                undofontstyle.Push(mainrichTextBox.SelectionFont.Style);
                undoactions.Push(mainrichTextBox.Text);
                isboldchecked = false;
                if (ItalictoolStripButton.Checked == true && UnderlinetoolStripButton.Checked == true && StrikethroughtoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Italic | FontStyle.Strikeout | FontStyle.Underline); boldtoolStripMenuItem.Checked = false; return; }
                if (ItalictoolStripButton.Checked == true && UnderlinetoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Italic | FontStyle.Underline); boldtoolStripMenuItem.Checked = false; return; }
                if (ItalictoolStripButton.Checked == true && StrikethroughtoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Italic | FontStyle.Strikeout); boldtoolStripMenuItem.Checked = false; return; }
                if (StrikethroughtoolStripButton.Checked == true && UnderlinetoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Strikeout | FontStyle.Underline); boldtoolStripMenuItem.Checked = false; return; }
                if (ItalictoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Italic); boldtoolStripMenuItem.Checked = false; return; }
                if (UnderlinetoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Underline); boldtoolStripMenuItem.Checked = false; return; }
                if (StrikethroughtoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Strikeout); boldtoolStripMenuItem.Checked = false; return; }

                mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Regular);
                boldtoolStripMenuItem.Checked = false;
            }
        }

        private void ItalictoolStripButton_Click(object sender, EventArgs e)
        {
            if (ItalictoolStripButton.Checked == true)
            {
                undoactions.Push(mainrichTextBox.Text);
                mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), mainrichTextBox.SelectionFont.Style | FontStyle.Italic);
                italicToolStripMenuItem.Checked = true;
                isitalicchecked = true;
            }
            else
            {
                undoactions.Push(mainrichTextBox.Text);
                isitalicchecked = false;
                if (BoldtoolStripButton.Checked == true && UnderlinetoolStripButton.Checked == true && StrikethroughtoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Bold | FontStyle.Strikeout | FontStyle.Underline); italicToolStripMenuItem.Checked = false; return; }
                if (BoldtoolStripButton.Checked == true && UnderlinetoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Bold | FontStyle.Underline); italicToolStripMenuItem.Checked = false; return; }
                if (BoldtoolStripButton.Checked == true && StrikethroughtoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Bold | FontStyle.Strikeout); italicToolStripMenuItem.Checked = false; return; }
                if (StrikethroughtoolStripButton.Checked == true && UnderlinetoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Strikeout | FontStyle.Underline); italicToolStripMenuItem.Checked = false; return; }
                if (BoldtoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Bold); italicToolStripMenuItem.Checked = false; return; }
                if (UnderlinetoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Underline); italicToolStripMenuItem.Checked = false; return; }
                if (StrikethroughtoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Strikeout); italicToolStripMenuItem.Checked = false; return; }

                mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text),  FontStyle.Regular);
                italicToolStripMenuItem.Checked = false;                
            }
        }

        private void UnderlinetoolStripButton_Click(object sender, EventArgs e)
        {
            if (UnderlinetoolStripButton.Checked == true)
            {
                undoactions.Push(mainrichTextBox.Text);
                mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), mainrichTextBox.SelectionFont.Style | FontStyle.Underline);
                underlineToolStripMenuItem.Checked = true;
                isunderlinechecked = true;
            }
            else
            {
                undoactions.Push(mainrichTextBox.Text);
                isunderlinechecked = false;
                if (BoldtoolStripButton.Checked == true && ItalictoolStripButton.Checked == true && StrikethroughtoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text),  FontStyle.Bold | FontStyle.Strikeout | FontStyle.Italic); underlineToolStripMenuItem.Checked = false; return; }
                if (BoldtoolStripButton.Checked == true && ItalictoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text),  FontStyle.Bold | FontStyle.Italic); underlineToolStripMenuItem.Checked = false; return; }
                if (BoldtoolStripButton.Checked == true && StrikethroughtoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Bold | FontStyle.Strikeout); underlineToolStripMenuItem.Checked = false; return; }
                if (StrikethroughtoolStripButton.Checked == true && ItalictoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Strikeout | FontStyle.Italic); underlineToolStripMenuItem.Checked = false; return; }
                if (BoldtoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Bold); underlineToolStripMenuItem.Checked = false; return; }
                if (ItalictoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Italic); underlineToolStripMenuItem.Checked = false; return; }
                if (StrikethroughtoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Strikeout); underlineToolStripMenuItem.Checked = false; return; }

                mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Regular);
                underlineToolStripMenuItem.Checked = false;
            }
        }

        private void StrikethroughtoolStripButton_Click(object sender, EventArgs e)
        {
            if (StrikethroughtoolStripButton.Checked == true)
            {
                undoactions.Push(mainrichTextBox.Text);
                mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), mainrichTextBox.SelectionFont.Style | FontStyle.Strikeout);
                strikethroughToolStripMenuItem.Checked = true;
                isstrikeoutchecked = true;
            }
            else
            {
                undoactions.Push(mainrichTextBox.Text);
                isstrikeoutchecked = false;
                if (BoldtoolStripButton.Checked == true && ItalictoolStripButton.Checked == true && UnderlinetoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Bold | FontStyle.Underline | FontStyle.Italic); strikethroughToolStripMenuItem.Checked = false; return; }
                if (BoldtoolStripButton.Checked == true && ItalictoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Bold | FontStyle.Italic); strikethroughToolStripMenuItem.Checked = false; return; }
                if (BoldtoolStripButton.Checked == true && UnderlinetoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Bold | FontStyle.Underline); strikethroughToolStripMenuItem.Checked = false; return; }
                if (UnderlinetoolStripButton.Checked == true && ItalictoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Underline | FontStyle.Italic); strikethroughToolStripMenuItem.Checked = false; return; }
                if (BoldtoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Bold); strikethroughToolStripMenuItem.Checked = false; return; }
                if (ItalictoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Italic); strikethroughToolStripMenuItem.Checked = false; return; }
                if (UnderlinetoolStripButton.Checked == true)
                { mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Underline); strikethroughToolStripMenuItem.Checked = false; return; }

                mainrichTextBox.SelectionFont = new Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), FontStyle.Regular);
                strikethroughToolStripMenuItem.Checked = false;
            }
        }

        private void FormatTexttoolStripButton_Click(object sender, EventArgs e)
        {
            formatFontToolStripMenuItem_Click(sender, e);
        }

        private void TextColortoolStripButton_Click(object sender, EventArgs e)
        {
            changeTextColorToolStripMenuItem_Click(sender, e);
        }

        private void PageColortoolStripButton_Click(object sender, EventArgs e)
        {
            ColorDialog colordialog = new ColorDialog();

            DialogResult result = colordialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                mainrichTextBox.BackColor = colordialog.Color;
            }
        }

        private void changePageColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PageColortoolStripButton_Click(sender, e);
        }

        private void mainrichTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (Control.IsKeyLocked(Keys.CapsLock))
            {
                CapstoolStripStatusLabel.Text = "Caps ON";
            }
            else
            {
                CapstoolStripStatusLabel.Text = "Caps OFF";
            }
            mainrichTextBox_Click(sender, e);
            undofontstyle.Push(mainrichTextBox.SelectionFont.Style);
            undoactions.Push(mainrichTextBox.Text);         
        }

        private void undoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            undoToolStripMenuItem_Click(sender, e);
        }

        private void redoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            redoToolStripMenuItem_Click(sender, e);
        }

        private void selectAllToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            selectAllToolStripMenuItem_Click(sender, e);
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (mainrichTextBox.SelectionLength > 0)
            {
                undoactions.Push(mainrichTextBox.Text);
                Clipboard.SetText(mainrichTextBox.SelectedText);
                mainrichTextBox.SelectedText = "";
            }
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (mainrichTextBox.SelectionLength > 0)
            {
                undoactions.Push(mainrichTextBox.Text);
                Clipboard.SetText(mainrichTextBox.SelectedText);
            }
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        { 
            if (Clipboard.ContainsText())
            {
                undoactions.Push(mainrichTextBox.Text);
                mainrichTextBox.SelectedText = Clipboard.GetText();
            }
        }

        private void cutToolStripMenuItem1_Click(object sender, EventArgs e)
        {            
            cutToolStripMenuItem_Click(sender, e);
        }

        private void copyToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            copyToolStripMenuItem_Click(sender, e);
        }

        private void pasteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            pasteToolStripMenuItem_Click(sender, e);
        }

        private void CuttoolStripButton_Click(object sender, EventArgs e)
        {
            cutToolStripMenuItem_Click(sender, e);
        }

        private void CopytoolStripButton_Click(object sender, EventArgs e)
        {
            copyToolStripMenuItem_Click(sender, e);
        }

        private void PastetoolStripButton_Click(object sender, EventArgs e)
        {
            pasteToolStripMenuItem_Click(sender, e);
        }

        private void deleteToolStripMenuItem1_Click(object sender, EventArgs e)
        {          
            if (mainrichTextBox.SelectionLength > 0)
                undoactions.Push(mainrichTextBox.Text);
                mainrichTextBox.SelectedText = "";
        }

        private void DeletetoolStripButton_Click(object sender, EventArgs e)
        {
            undoactions.Push(mainrichTextBox.Text);
            if (mainrichTextBox.SelectionLength > 0)
                mainrichTextBox.SelectedText = "";
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            undoactions.Push(mainrichTextBox.Text);
            if (mainrichTextBox.SelectionLength > 0)
                mainrichTextBox.SelectedText = "";
        }

        private void enabledToolStripMenuItem_Click(object sender, EventArgs e)
        {
           //SpeechToText();
           ss.SelectVoiceByHints(VoiceGender.Female);
           ss.SpeakAsync("Now i am listening to you");
           enabledToolStripMenuItem.Enabled = false;
           disabledToolStripMenuItem.Enabled = true;
           clist.Add(new string[] { "create new document", "open new file", "save this", "save this as", "close this",
                                    "take print", "print preview", "select all", "clear all", "make bold", "make italic",
                                    "make underline", "make strikethrough", "make normal", "remove bold", "remove italic",
                                    "remove underline", "remove strikethrough", "perform cut", "perform copy", "get datetime", 
                                    "perform paste", "perform del", "perform undo", "perform redo", "stop listening", "unselect all",
                                    "give space", "give back space", "give new line", "increase font size", "decrease font size", "more",
                                    "change text color", "change page color", "change font", "apply", "cancel", "one", "two", "three", "four", "five",
                                    "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen",
                                    "seventeen", "eighteen", "nineteen", "twenty", "twentyone", "twentytwo", "twentythree", "twentyfour",
                                    "twentyfive", "twentysix", "twentyseven", "twentyeight", "twentynine", "thirty", "wait", "listen", "start reading",
                                    "find", "got it"});

            clist.Add(choices);
            //clist.Add(words);

            Grammar gr = new Grammar(new GrammarBuilder(clist));

           try
           {
               sre.RequestRecognizerUpdate();
               sre.LoadGrammar(gr);
               sre.SpeechRecognized += sre_SpeechRecognized;
               sre.SetInputToDefaultAudioDevice();
               sre.RecognizeAsync(RecognizeMode.Multiple);

           }
           catch (Exception ex)
           {
                MessageBox.Show(ex.Message, "Error");
           }

       }

        private void SpeechToText()
        {
            string path = Environment.CurrentDirectory + "/" + "dictionary.txt";

            using (StreamReader sr = new StreamReader(path))
            {
                string text = sr.ReadToEnd();
                words = text.Split('\n');
            }
        }

        void sre_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
       {
           /* foreach (string word in words)
            {
                if(word == e.Result.Text.ToString())
                {
                    mainrichTextBox.Text += " " + word;
                    mainrichTextBox.SelectionStart = mainrichTextBox.TextLength;
                }
            } */

        // mainrichTextBox.Text = e.Result.Text.ToString();

         if (pause != true)
         {
            if (find == true)
            {
             if(dialogappear == false)
              { 
                find = false;

                foreach (string ch in choices)
                {
                   if (ch == e.Result.Text.ToString())
                   {
                     int start = 0;
                     int end = mainrichTextBox.Text.LastIndexOf(ch);
                     mainrichTextBox.Font = new System.Drawing.Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), bold | italic | underline | strikeout);
                     if (textcolor == true)
                        { mainrichTextBox.ForeColor = fontcolor; }
                     String temp = mainrichTextBox.Text;
                     mainrichTextBox.Text = "";
                     mainrichTextBox.Text = temp; 
                                          
                     while (start < end)
                     {
                        mainrichTextBox.Find(ch, start, mainrichTextBox.TextLength, RichTextBoxFinds.None);                     
                        mainrichTextBox.SelectionBackColor = Color.Pink;

                        if(pagecolor != "White")
                          { mainrichTextBox.SelectionBackColor = Color.LightGray ; }
                        if(fontcolor != Color.Black)
                          { mainrichTextBox.SelectionBackColor = Color.LightGray; }

                         start = mainrichTextBox.Text.IndexOf(ch, start) + 1;
                     }
                        mainrichTextBox.SelectionStart = mainrichTextBox.TextLength;
                   }
                 }
               }
            }


             if (e.Result.ToString() != "apply")
             {
                blacklabel.BackColor = Color.White;
                whitelabel.BackColor = Color.White;
                redlabel.BackColor = Color.White;
                maroonlabel.BackColor = Color.White;
                pinklabel.BackColor = Color.White;
                bluelabel.BackColor = Color.White;
                lightbluelabel.BackColor = Color.White;
                greenlabel.BackColor = Color.White;
                lightgreenlabel.BackColor = Color.White;
                yellowlabel.BackColor = Color.White;
                brownlabel.BackColor = Color.White;
                purplelabel.BackColor = Color.White;
                orangelabel.BackColor = Color.White;
                indigolabel.BackColor = Color.White;
                greylabel.BackColor = Color.White;
                label2.BackColor = Color.White;
                label3.BackColor = Color.White;
                label4.BackColor = Color.White;
                label5.BackColor = Color.White;
                label6.BackColor = Color.White;
                label7.BackColor = Color.White;
                label8.BackColor = Color.White;
                label9.BackColor = Color.White;
                label10.BackColor = Color.White;
                label11.BackColor = Color.White;
                label12.BackColor = Color.White;
                label13.BackColor = Color.White;
                label14.BackColor = Color.White;
                label15.BackColor = Color.White;
                label49.BackColor = Color.White;
                label50.BackColor = Color.White;
                label51.BackColor = Color.White;
                label52.BackColor = Color.White;
                label53.BackColor = Color.White;
                label54.BackColor = Color.White;
                label55.BackColor = Color.White;
                label56.BackColor = Color.White;
                label57.BackColor = Color.White;
                label58.BackColor = Color.White;
                label59.BackColor = Color.White;
                label60.BackColor = Color.White;
                label61.BackColor = Color.White;
                label62.BackColor = Color.White;
                label63.BackColor = Color.White;
                label64.BackColor = Color.White;
             }

            switch (e.Result.Text.ToString())
           {
               case "create new document":
                    if (dialogappear == false)
                        newToolStripMenuItem_Click(sender, e);
                   break;

               case "open new file":
                    if (dialogappear == false)
                        openToolStripMenuItem_Click(sender, e);                   
                   break;

               case "save this":
                    if (dialogappear == false)
                        saveToolStripMenuItem_Click(sender, e);                    
                   break;

               case "save this as":
                    if (dialogappear == false)
                        saveAsToolStripMenuItem_Click(sender, e);                    
                   break;

               case "close this":
                    if (dialogappear == false)
                        Application.Exit();
                    break;

               case "clear all":
                    if (dialogappear == false)
                    { ss.SpeakAsync("cleared!");
                    undoactions.Push(mainrichTextBox.Text);
                        mainrichTextBox.Clear();
                    }
                    break;

               case "select all":
                    if (dialogappear == false)
                    { ss.SpeakAsync("all text selected!");
                    selectAllToolStripMenuItem_Click(sender, e);
                    }
                    break;

                case "unselect all":
                    if (dialogappear == false)
                    { ss.SpeakAsync("okay!");                   
                    mainrichTextBox.SelectedText = mainrichTextBox.SelectedText;
                    mainrichTextBox_Click(sender, e);
                    }
                    break;

                case "make bold":
                    if (dialogappear == false)
                    {  ss.SpeakAsync("done!");
                    boldtoolStripMenuItem.Checked = true;
                    boldtoolStripMenuItem_Click(sender, e);
                    bold = FontStyle.Bold;
                    }
                    break;

                case "make italic":
                    if (dialogappear == false)
                    {  ss.SpeakAsync("done!");
                    italicToolStripMenuItem.Checked = true;
                    italicToolStripMenuItem_Click(sender, e);
                    italic = FontStyle.Italic;
                    }
                    break;

                case "make underline":
                    if (dialogappear == false)
                    { ss.SpeakAsync("done!");
                    underlineToolStripMenuItem.Checked = true;
                    underlineToolStripMenuItem_Click(sender, e);
                    underline = FontStyle.Underline;
                    }
                    break;

                case "make strikethrough":
                    if (dialogappear == false)
                    {  ss.SpeakAsync("done!");
                    strikethroughToolStripMenuItem.Checked = true;
                    strikethroughToolStripMenuItem_Click(sender, e);
                    strikeout = FontStyle.Strikeout;
                    }
                    break;
                        
                case "remove bold":
                    if (dialogappear == false)
                    {   ss.SpeakAsync("done!");
                    boldtoolStripMenuItem.Checked = false;
                    boldtoolStripMenuItem_Click(sender, e);
                    bold = FontStyle.Regular;
                    }
                    break;

                case "remove italic":
                    if (dialogappear == false)
                    { ss.SpeakAsync("done!");
                    italicToolStripMenuItem.Checked = false;
                    italicToolStripMenuItem_Click(sender, e);
                    italic = FontStyle.Regular;
                    }
                    break;

                case "remove underline":
                    if (dialogappear == false)
                    { ss.SpeakAsync("done!");
                    underlineToolStripMenuItem.Checked = false;
                    underlineToolStripMenuItem_Click(sender, e);
                    underline = FontStyle.Regular;
                    }
                    break;

                case "remove strikethrough":
                    if (dialogappear == false)
                    { ss.SpeakAsync("done!");
                    strikethroughToolStripMenuItem.Checked = false;
                    strikethroughToolStripMenuItem_Click(sender, e);
                    strikeout = FontStyle.Regular;
                    }
                    break;

                case "make normal":
                    if (dialogappear == false)
                    { ss.SpeakAsync("done!");
                    normalToolStripMenuItem_Click(sender, e);
                    }
                    break;

                case "perform cut":
                    if (dialogappear == false)
                    {  ss.SpeakAsync("okay!");
                    cutToolStripMenuItem_Click(sender, e);
                    }
                    break;
                    
                case "perform copy":
                    if (dialogappear == false)
                    {  ss.SpeakAsync("okay!");
                    copyToolStripMenuItem_Click(sender, e);
                    }
                    break;

                case "perform paste":
                    if (dialogappear == false)
                    { ss.SpeakAsync("done!");
                    pasteToolStripMenuItem_Click(sender, e);
                    }
                    break;

                case "perform del":
                    if (dialogappear == false)
                    {  ss.SpeakAsync("done!");
                    DeletetoolStripButton_Click(sender, e);
                    }
                    break;

                case "perform undo":
                    if (dialogappear == false)
                    { if (UndotoolStripButton.Enabled == true)
                    { ss.SpeakAsync("done!"); }
                    else { ss.SpeakAsync("No, nothing exist"); }
                    undoToolStripMenuItem_Click(sender, e);
                    }
                    break;

                case "perform redo":
                    if (dialogappear == false)
                    { if (RedotoolStripButton.Enabled == true)
                    { ss.SpeakAsync("done!"); }
                    else { ss.SpeakAsync("No, nothing exist"); }
                    redoToolStripMenuItem_Click(sender, e);
                    }
                    break;

                case "get datetime":
                    if (dialogappear == false)
                    { ss.SpeakAsync("done!");
                    undoactions.Push(mainrichTextBox.Text);
                    mainrichTextBox.SelectedText = DateTime.Now.ToString();
                    }
                    break;

                case "stop listening":
                    if (dialogappear == false)
                    { 
                     disabledToolStripMenuItem_Click(sender, e);
                    }
                    break;

                case "wait":
                    ss.SpeakAsync("okay sir, i am waiting");
                    pause = true;
                    break;

                case "give space":
                    if (dialogappear == false)
                    { ss.SpeakAsync("done!");
                    undoactions.Push(mainrichTextBox.Text);
                    mainrichTextBox.Text += " ";
                    mainrichTextBox.SelectionStart = mainrichTextBox.Text.Length;
                    }
                    break;

                case "give back space":
                    if (dialogappear == false)
                    {  ss.SpeakAsync("done!");
                    undoToolStripMenuItem_Click(sender, e);
                    }
                    break;

                case "start reading":
                        if (dialogappear == false)
                        {
                            ss.Rate = -1;       
                            ss.SpeakAsync(mainrichTextBox.Text);
                        }
                        break;

                case "find":
                        if (dialogappear == false)
                        {
                            ss.SpeakAsync("what sir?");
                            find = true;
                        }
                        break;

                case "got it":
                        if (dialogappear == false)
                        {
                            mainrichTextBox.Font = new System.Drawing.Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text), bold | italic | underline | strikeout);
                        if (textcolor == true)
                        { mainrichTextBox.ForeColor = fontcolor; }
                        String temp = mainrichTextBox.Text;
                        mainrichTextBox.Text = "";
                        mainrichTextBox.Text = temp;
                        }
                        break;

                case "give new line":
                    if (dialogappear == false)
                    { ss.SpeakAsync("done!");
                    undoactions.Push(mainrichTextBox.Text);
                    mainrichTextBox.Text += Environment.NewLine;                    
                    mainrichTextBox.SelectionStart = mainrichTextBox.Text.Length;
                    }
                    break;

                case "increase font size":
                    if (dialogappear == false)
                    {                      
                        increasemore = true;
                        decreasemore = false;
                        int size = int.Parse(fontsizetoolStripComboBox.Text);
                        size += 2;
                        if (size > 72)
                        { ss.SpeakAsync("No, text already have maximum size"); }
                        else
                        { ss.SpeakAsync("okay!");
                        fontsizetoolStripComboBox.Text = size.ToString();
                        }
                    }
                    break;

                case "decrease font size":
                    if (dialogappear == false)
                    {
                        ss.SpeakAsync("okay!");
                        decreasemore = true;
                        increasemore = false;
                        int size = int.Parse(fontsizetoolStripComboBox.Text);
                        size -= 2;
                        if (size < 8)
                        { ss.SpeakAsync("No, text already have minimum size"); }
                        else
                        {
                            ss.SpeakAsync("okay!");
                            fontsizetoolStripComboBox.Text = size.ToString();
                        }
                    }
                    break;

                case "more":
                    if (dialogappear == false && increasemore == true)
                    {
                        int size = int.Parse(fontsizetoolStripComboBox.Text);
                        size += 2;
                        if (size > 72)
                        { ss.SpeakAsync("No, text already have maximum size"); }
                        else
                        {
                            ss.SpeakAsync("done!");
                            fontsizetoolStripComboBox.Text = size.ToString();
                        }
                    }
                    if (dialogappear == false && decreasemore == true)
                    {
                        int size = int.Parse(fontsizetoolStripComboBox.Text);
                        size -= 2;
                        if (size < 8)
                        { ss.SpeakAsync("No, text already have minimum size"); }
                        else
                        {
                            ss.SpeakAsync("done!");
                            fontsizetoolStripComboBox.Text = size.ToString();
                        }
                    }
                    break;

                case "change page color":
                     if (dialogappear == false)
                    { ss.SpeakAsync("Opening Color dialog");
                    colorpanel.Visible = true;
                    dialogappear = true;
                    colorbox = true;
                    textcolor = false;
                    }
                    break;

                case "change text color":
                    if (dialogappear == false)
                    { ss.SpeakAsync("Opening Color dialog");
                    colorpanel.Visible = true;
                    dialogappear = true;
                    colorbox = true;
                    textcolor = true;
                    }
                    break;

                case "change font":
                    if (dialogappear == false)
                    {  ss.SpeakAsync("Opening font dialog");
                    fontpanel.Visible = true;
                    changingfont = true;
                    dialogappear = true;
                    }
                    break;

                case "one":
                    if (colorbox == true)
                    { ss.SpeakAsync("okay!");
                    blacklabel.BackColor = Color.LightGray;
                    pagecolor = "Black";
                    fontcolor = Color.Black;
                    }
                    nameoffont = "Agency FB";
                    lblname = label49;
                    changefont(nameoffont, lblname);
                    break;

                case "two":
                    if (colorbox == true)
                    {  ss.SpeakAsync("okay!");
                    whitelabel.BackColor = Color.LightGray;
                    pagecolor = "White";
                    fontcolor = Color.White;
                        }
                    nameoffont = "Algerian";
                    lblname = label15;
                    changefont(nameoffont, lblname);
                    break;                   

                case "three":
                    if (colorbox == true)
                    {   ss.SpeakAsync("okay!");
                    redlabel.BackColor = Color.LightGray;
                            pagecolor = "Red";
                            fontcolor = Color.Red;
                        }
                    nameoffont = "Arial";
                    lblname = label13;
                    changefont(nameoffont, lblname);
                    break;

                case "four":
                    if (colorbox == true)
                    {  ss.SpeakAsync("okay!");
                    maroonlabel.BackColor = Color.LightGray;
                            pagecolor = "Maroon";
                            fontcolor = Color.Maroon;
                        }
                    nameoffont = "Modern No. 20";
                    lblname = label10;
                    changefont(nameoffont, lblname);
                    break;

                case "five":
                    if (colorbox == true)
                    {  ss.SpeakAsync("okay!");
                    pinklabel.BackColor = Color.LightGray;
                            pagecolor = "Pink";
                            fontcolor = Color.Pink;
                        }
                    nameoffont = "Baskerville Old Face";
                    lblname = label8;
                    changefont(nameoffont, lblname);
                    break;

                case "six":
                    if (colorbox == true)
                    {  ss.SpeakAsync("okay!");
                    bluelabel.BackColor = Color.LightGray;
                            pagecolor = "Blue";
                            fontcolor = Color.Blue;
                        }
                    nameoffont = "Bauhaus 93";
                    lblname = label4;
                    changefont(nameoffont, lblname);
                    break;

                case "seven":
                    if (colorbox == true)
                    {   ss.SpeakAsync("okay!");
                    lightbluelabel.BackColor = Color.LightGray;
                            pagecolor = "LightBlue";
                            fontcolor = Color.LightBlue;
                        }
                    nameoffont = "Bell MT";
                    lblname = label14;
                    changefont(nameoffont, lblname);
                    break;

                case "eight":
                    if (colorbox == true)
                    {   ss.SpeakAsync("okay!");
                    greenlabel.BackColor = Color.LightGray;
                            pagecolor = "Green";
                            fontcolor = Color.Green;
                        }
                    nameoffont = "Berlin Sans FB";
                    lblname = label12;
                    changefont(nameoffont, lblname);
                    break;

                case "nine":
                    if (colorbox == true)
                    {   ss.SpeakAsync("okay!");
                    lightgreenlabel.BackColor = Color.LightGray;
                            pagecolor = "LightGreen";
                            fontcolor = Color.LightGreen;
                        }
                    nameoffont = "Bodoni MT";
                    lblname = label11;
                    changefont(nameoffont, lblname);
                    break;

                case "ten":
                    if (colorbox == true)
                    { ss.SpeakAsync("okay!");
                    yellowlabel.BackColor = Color.LightGray;
                            pagecolor = "Yellow";
                            fontcolor = Color.Yellow;
                        }
                    nameoffont = "Blackadder ITC";
                    lblname = label9;
                    changefont(nameoffont, lblname);
                    break;

                case "eleven":
                    if (colorbox == true)
                    {  ss.SpeakAsync("okay!");
                    brownlabel.BackColor = Color.LightGray;
                            pagecolor = "Brown";
                            fontcolor = Color.Brown;
                        }
                    nameoffont = "Book Antiqua";
                    lblname = label6;
                    changefont(nameoffont, lblname);
                    break;

                case "twelve":
                    if (colorbox == true)
                    {   ss.SpeakAsync("okay!");
                    purplelabel.BackColor = Color.LightGray;
                            pagecolor = "Purple";
                            fontcolor = Color.Purple;
                        }
                    nameoffont = "Bookman Old Style";
                    lblname = label3;
                    changefont(nameoffont, lblname);
                    break;

                case "thirteen":
                    if (colorbox == true)
                    { ss.SpeakAsync("okay!");
                    orangelabel.BackColor = Color.LightGray;
                    pagecolor = "Orange";
                    fontcolor = Color.Orange;
                        }
                    nameoffont = "Bradley Hand ITC";
                    lblname = label7;
                    changefont(nameoffont, lblname);
                    break;

                case "fourteen":
                    if (colorbox == true)
                    { ss.SpeakAsync("okay!");
                    indigolabel.BackColor = Color.LightGray;
                    pagecolor = "Indigo";
                    fontcolor = Color.Indigo;
                        }
                    nameoffont = "Bookman Old Style";
                    lblname = label5;
                    changefont(nameoffont, lblname);
                    break;

                case "fifteen":
                    if (colorbox == true)
                    { ss.SpeakAsync("okay!");
                    greylabel.BackColor = Color.LightGray;
                    pagecolor = "Gray";
                    fontcolor = Color.Gray;
                        }
                    nameoffont = "Broadway";
                    lblname = label2;
                    changefont(nameoffont, lblname);
                    break;

                case "sixteen":
                    nameoffont = "Calibri";
                    lblname = label50;
                    changefont(nameoffont, lblname);
                    break;

                case "seventeen":
                    nameoffont = "Consolas";
                    lblname = label51;
                    changefont(nameoffont, lblname);
                    break;

                case "eighteen":
                    nameoffont = "Century";
                    lblname = label53;
                    changefont(nameoffont, lblname);
                    break;

                case "nineteen":
                    nameoffont = "Chiller";
                    lblname = label56;
                    changefont(nameoffont, lblname);
                    break;

                case "twenty":
                    nameoffont = "Cooper";
                    lblname = label58;
                    changefont(nameoffont, lblname);
                    break;

                case "twentyone":
                    nameoffont = "Curlz MT";
                    lblname = label62;
                    changefont(nameoffont, lblname);
                    break;

                case "twentytwo":
                    nameoffont = "Elephant";
                    lblname = label52;
                    changefont(nameoffont, lblname);
                    break;

                case "twentythree":
                    nameoffont = "Forte";
                    lblname = label54;
                    changefont(nameoffont, lblname);
                    break;

                case "twentyfour":
                    nameoffont = "Garamond";
                    lblname = label55;
                    changefont(nameoffont, lblname);
                    break;

                case "twentyfive":
                    nameoffont = "High Tower Text";
                    lblname = label57;
                    changefont(nameoffont, lblname);
                    break;

                case "twentysix":
                    nameoffont = "Jokerman";
                    lblname = label60;
                    changefont(nameoffont, lblname);
                    break;

                case "twentyseven":
                    nameoffont = "Kristen ITC";
                    lblname = label63;
                    changefont(nameoffont, lblname);
                    break;

                case "twentyeight":
                    nameoffont = "Lucida Calligraphy";
                    lblname = label59;
                    changefont(nameoffont, lblname);
                    break;

                case "twentynine":
                    nameoffont = "Microsoft Sans Serif";
                    lblname = label61;
                    changefont(nameoffont, lblname);
                    break;

                case "thirty":
                    nameoffont = "Segoe UI";
                    lblname = label64;
                    changefont(nameoffont, lblname);
                    break;

                case "cancel":
                    if (colorbox == true || changingfont == true)
                        ss.SpeakAsync("okay!");
                    colorpanel.Visible = false;
                    fontpanel.Visible = false;
                    dialogappear = false;
                    changingfont = false;
                    colorbox = false;
                    break;

                case "apply":
                    if (colorbox == true)
                    {
                        ss.SpeakAsync("okay!");
                        if (textcolor == false)
                        {
                            if (pagecolor == "Black")
                                mainrichTextBox.BackColor = Color.Black;
                            if (pagecolor == "White")
                                mainrichTextBox.BackColor = Color.White;
                            if (pagecolor == "Red")
                                mainrichTextBox.BackColor = Color.Red;
                            if (pagecolor == "Pink")
                                mainrichTextBox.BackColor = Color.Pink;
                            if (pagecolor == "Blue")
                                mainrichTextBox.BackColor = Color.Blue;
                            if (pagecolor == "LightBlue")
                                mainrichTextBox.BackColor = Color.LightBlue;
                            if (pagecolor == "Green")
                                mainrichTextBox.BackColor = Color.Green;
                            if (pagecolor == "LightGreen")
                                mainrichTextBox.BackColor = Color.LightGreen;
                            if (pagecolor == "Yellow")
                                mainrichTextBox.BackColor = Color.Yellow;
                            if (pagecolor == "Brown")
                                mainrichTextBox.BackColor = Color.Brown;
                            if (pagecolor == "Purple")
                                mainrichTextBox.BackColor = Color.Purple;
                            if (pagecolor == "Indigo")
                                mainrichTextBox.BackColor = Color.Indigo;
                            if (pagecolor == "Orange")
                                mainrichTextBox.BackColor = Color.Orange;
                            if (pagecolor == "Gray")
                                mainrichTextBox.BackColor = Color.Gray;
                            if (pagecolor == "Maroon")
                                mainrichTextBox.BackColor = Color.Maroon;
                        }
                        else
                        {
                            if (fontcolor == Color.Black)
                                mainrichTextBox.SelectionColor = Color.Black;
                            if (fontcolor == Color.White)
                                mainrichTextBox.SelectionColor = Color.White;
                            if (fontcolor == Color.Red)
                                mainrichTextBox.SelectionColor = Color.Red;
                            if (fontcolor == Color.Pink)
                                mainrichTextBox.SelectionColor = Color.Pink;
                            if (fontcolor == Color.Blue)
                                mainrichTextBox.SelectionColor = Color.Blue;
                            if (fontcolor == Color.LightBlue)
                                mainrichTextBox.SelectionColor = Color.LightBlue;
                            if (fontcolor == Color.Green)
                                mainrichTextBox.SelectionColor = Color.Green;
                            if (fontcolor == Color.LightGreen)
                                mainrichTextBox.SelectionColor = Color.LightGreen;
                            if (fontcolor == Color.Yellow)
                                mainrichTextBox.SelectionColor = Color.Yellow;
                            if (fontcolor == Color.Brown)
                                mainrichTextBox.SelectionColor = Color.Brown;
                            if (fontcolor == Color.Purple)
                                mainrichTextBox.SelectionColor = Color.Purple;
                            if (fontcolor == Color.Indigo)
                                mainrichTextBox.SelectionColor = Color.Indigo;
                            if (fontcolor == Color.Orange)
                                mainrichTextBox.SelectionColor = Color.Orange;
                            if (fontcolor == Color.Gray)
                                mainrichTextBox.SelectionColor = Color.Gray;
                            if (fontcolor == Color.Maroon)
                                mainrichTextBox.SelectionColor = Color.Maroon;

                            mainrichTextBox.SelectedText = mainrichTextBox.SelectedText;
                        }

                        colorpanel.Visible = false;
                        colorbox = false;
                        dialogappear = false;
                    }

                    if (changingfont == true)
                    {
                        ss.SpeakAsync("okay!");
                        fontnametoolStripComboBox.Text = nameoffont;
                        fontpanel.Visible = false;
                        dialogappear = false;
                        changingfont = false;
                        mainrichTextBox.SelectedText = mainrichTextBox.SelectedText;
                    }

                    break;
            }
          }
          if (e.Result.Text.ToString() == "listen")
            {
                ss.SpeakAsync("yes Sir!");
                pause = false;
            }
        }

        private void changefont(string nameoffont, Label lblname)
        {
            if (changingfont == true)
                ss.SpeakAsync("okay!");
            lblname.BackColor = Color.LightGray;
            choosenfont = nameoffont;
        }

        private void disabledToolStripMenuItem_Click(object sender, EventArgs e)
        {
             ss.SpeakAsync("good bye sir!");
             sre.RecognizeAsyncStop();
             enabledToolStripMenuItem.Enabled = true;
             disabledToolStripMenuItem.Enabled = false; 
        }

        private void mainrichTextBox_Click(object sender, EventArgs e)
        {
            string[] fontname = { "Agency FB","Algerian","Arial","Arial Rounded MT","Arial Unicode MS","Baskerville Old Face","Bauhaus 93","Bell MT","Berlin Sans FB","Bernard MT Condensed","Blackadder ITC","Bodoni MT","Bodoni MT Condensed","Bodoni MT Poster Compressed",
                                  "Book Antiqua","Bookman Old Style","Bradley Hand ITC","Broadway","Brush Script MT","Calibri","Californian FB","Calisto MT","Cambria & Cambria Math","Candara","Centaur","Century","Century Gothic","Century Schoolbook","Chiller","Colonna MT","Consolas","Constantia","Cooper Black","Copperplate Gothic",
                                  "Corbel","Curlz MT","Edwardian Script ITC","Elephant","Engraveras","Eras Bold ITC","Eras Demi ITC","Eras Light ITC","Eras Medium ITC","Felix Titling","Footlight MT Light","Forte","Franklin","Franklin Gothic","Franklin Gothic Demi","Franklin Gothic Demi Cond","Franklin Gothic Heavy","Freestyle Script",
                                  "French Script MT","Garamond","Gigi","Gill Sans MT","Gill Sans MT Condensed","Gill Sans MT Ext Condensed","Gill Sans Ultra","Gloucester MT Extra Condensed","Gothic Book","Goudy Old Style","Goudy Stout","Haettenschweiler","Harlow Solid","High Tower Text","Imprint MT Shadow","Informal Roman",
                                  "Jokerman","Juice ITC","Kristen ITC","Kunstler Script","Lucida Bright","Lucida Bright Demibold","Lucida Calligraphy","Lucida Fax","Lucida Handwriting","Lucida Sans","Lucida Sans Demibold Roman","Lucida Sans Typewriter","Magneto","Maiandra GD","Matura MT Script Capitals","Mistral","Modern No. 20",
                                  "Monotype Corsiva","MS Mincho","Microsoft Sans Serif","MS Outlook","MS Reference Sans Serif","MS Reference Specialty","MT Extra","Niagara Engraved","Niagara Solid","OCR A Extended","Old English Text MT","Onyx","Palace Script MT","Palatino Linotype","Parchment","Perpetua","Playbill","Poor Richard","Pristina","Rage","Ravie Rockwell",
                                  "Rockwell","Rockwell Condensed","Segoe UI*","Showcard Gothic","Snap ITC","Stencil","Tempus Sans ITC","Tw Cen MT","Tw Cen MT Condensed","Viner Hand ITC","Vladimir Script","Wide Latin","Wingdings 2","Wingdings 3" };
                  
            if (mainrichTextBox.SelectionLength > 0)
            {
                 for (int i = 8; i < 73; i++) 
                 {
                     if (mainrichTextBox.SelectionFont.Size == i)
                     {
                         fontsizetoolStripComboBox.Text = i.ToString();
                         break;
                     }                  
                 }

                 for (int i = 0; i < 121; i++)
                 {
                     if (mainrichTextBox.SelectionFont.Name == fontname[i])
                     {
                         fontnametoolStripComboBox.Text = fontname[i];
                         break;
                     }
                 }               

                BoldtoolStripButton.Checked = false;
                ItalictoolStripButton.Checked = false;
                UnderlinetoolStripButton.Checked = false;
                StrikethroughtoolStripButton.Checked = false;

                CutCopyDelItems(true);

                if (mainrichTextBox.SelectionFont.Style.ToString() == "Regular")
                   { BoldtoolStripButton.Checked = false; ItalictoolStripButton.Checked = false; UnderlinetoolStripButton.Checked = false; StrikethroughtoolStripButton.Checked = false; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Bold")
                   { BoldtoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Italic")
                   { ItalictoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Underline")
                   { UnderlinetoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout")
                   { StrikethroughtoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Italic" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Bold" )
                   { BoldtoolStripButton.Checked = true; ItalictoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Bold")
                   { BoldtoolStripButton.Checked = true; UnderlinetoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Bold")
                   { BoldtoolStripButton.Checked = true; StrikethroughtoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Italic")
                   { ItalictoolStripButton.Checked = true; UnderlinetoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Italic")
                   { ItalictoolStripButton.Checked = true; StrikethroughtoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Underline")
                   { UnderlinetoolStripButton.Checked = true; StrikethroughtoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Italic, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Underline, Italic" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Bold, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Italic, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Underline, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Bold, Italic")
                   { BoldtoolStripButton.Checked = true; ItalictoolStripButton.Checked = true; UnderlinetoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Italic, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Underline, Italic" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Strikeout, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Italic, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Underline, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Strikeout, Italic")
                   {StrikethroughtoolStripButton.Checked = true; ItalictoolStripButton.Checked = true; UnderlinetoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Bold, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Underline, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Strikeout, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Bold, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Underline, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Strikeout, Bold")
                   { BoldtoolStripButton.Checked = true; StrikethroughtoolStripButton.Checked = true; UnderlinetoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Italic, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Italic, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Strikeout, Italic" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Bold, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Italic, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Strikeout, Bold")
                   { BoldtoolStripButton.Checked = true; ItalictoolStripButton.Checked = true; StrikethroughtoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Italic, Underline, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Strikeout, Italic, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Bold, Strikeout, Italic" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Italic, Bold, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Bold, Italic, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Italic, Strikeout, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Strikeout, Italic, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Underline, Strikeout, Italic" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Italic, Underline, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Underline, Italic, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Strikeout, Underline, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Bold, Strikeout, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Underline, Bold, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Bold, Underline, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Underline, Strikeout, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Underline, Italic, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Bold, Underline, Italic" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Italic, Bold, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Bold, Italic, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Italic, Underline, Bold")
                   { BoldtoolStripButton.Checked = true; ItalictoolStripButton.Checked = true; UnderlinetoolStripButton.Checked = true; StrikethroughtoolStripButton.Checked = true; return; }
                                  
            }

            else
            {
                if (fontsizetoolStripComboBox.Text == "")
                {
                    fontsizetoolStripComboBox.Text = 12.ToString();
                }
                if (int.Parse(fontsizetoolStripComboBox.Text) < 8)
                {
                    fontsizetoolStripComboBox.Text = 8.ToString();
                }
                if (fontnametoolStripComboBox.Text == "")
                {
                    fontnametoolStripComboBox.Text = "Microsoft Sans Serif";
                }

                if (isfontsizeselected == true)
                {
                    mainrichTextBox.SelectionFont = new System.Drawing.Font(mainrichTextBox.Font.Name, int.Parse(fontsizetoolStripComboBox.Text));
                }

                for (int i = 8; i < 73; i++)
                {
                    if (mainrichTextBox.SelectionFont.Size == i)
                    {
                        fontsizetoolStripComboBox.Text = i.ToString();
                        break;
                    }
                }

                if (isfontnameselected == true)
                {
                    mainrichTextBox.SelectionFont = new System.Drawing.Font(fontnametoolStripComboBox.Text, mainrichTextBox.Font.Size);
                }

                for (int i = 0; i < 121; i++)
                {
                    if (mainrichTextBox.SelectionFont.Name == fontname[i])
                    {
                        fontnametoolStripComboBox.Text = fontname[i];
                        break;
                    }                   
                }

                isfontsizeselected = false;
                isfontnameselected = false;

                BoldtoolStripButton.Checked = false;
                ItalictoolStripButton.Checked = false;
                UnderlinetoolStripButton.Checked = false;
                StrikethroughtoolStripButton.Checked = false;

                CutCopyDelItems(false);

                mainrichTextBox.SelectionFont = new System.Drawing.Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text));    

                if (mainrichTextBox.SelectionFont.Style.ToString() == "Regular")
                { BoldtoolStripButton.Checked = false; ItalictoolStripButton.Checked = false; UnderlinetoolStripButton.Checked = false; StrikethroughtoolStripButton.Checked = false; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Bold")
                { BoldtoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Italic")
                { ItalictoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Underline")
                { UnderlinetoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout")
                { StrikethroughtoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Italic" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Bold")
                { BoldtoolStripButton.Checked = true; ItalictoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Bold")
                { BoldtoolStripButton.Checked = true; UnderlinetoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Bold")
                { BoldtoolStripButton.Checked = true; StrikethroughtoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Italic")
                { ItalictoolStripButton.Checked = true; UnderlinetoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Italic")
                { ItalictoolStripButton.Checked = true; StrikethroughtoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Underline")
                { UnderlinetoolStripButton.Checked = true; StrikethroughtoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Italic, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Underline, Italic" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Bold, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Italic, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Underline, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Bold, Italic")
                { BoldtoolStripButton.Checked = true; ItalictoolStripButton.Checked = true; UnderlinetoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Italic, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Underline, Italic" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Strikeout, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Italic, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Underline, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Strikeout, Italic")
                { StrikethroughtoolStripButton.Checked = true; ItalictoolStripButton.Checked = true; UnderlinetoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Bold, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Underline, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Strikeout, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Bold, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Underline, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Strikeout, Bold")
                { BoldtoolStripButton.Checked = true; StrikethroughtoolStripButton.Checked = true; UnderlinetoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Italic, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Italic, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Strikeout, Italic" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Bold, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Italic, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Strikeout, Bold")
                { BoldtoolStripButton.Checked = true; ItalictoolStripButton.Checked = true; StrikethroughtoolStripButton.Checked = true; return; }
                if (mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Italic, Underline, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Strikeout, Italic, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Bold, Strikeout, Italic" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Italic, Bold, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Bold, Italic, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Underline, Italic, Strikeout, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Strikeout, Italic, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Underline, Strikeout, Italic" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Italic, Underline, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Bold, Underline, Italic, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Strikeout, Underline, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Bold, Strikeout, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Underline, Bold, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Bold, Underline, Strikeout" || mainrichTextBox.SelectionFont.Style.ToString() == "Italic, Underline, Strikeout, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Underline, Italic, Bold" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Bold, Underline, Italic" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Italic, Bold, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Bold, Italic, Underline" || mainrichTextBox.SelectionFont.Style.ToString() == "Strikeout, Italic, Underline, Bold")
                { BoldtoolStripButton.Checked = true; ItalictoolStripButton.Checked = true; UnderlinetoolStripButton.Checked = true; StrikethroughtoolStripButton.Checked = true; return; }


                if (isboldchecked == true)
                    BoldtoolStripButton.Checked = true;
                else
                    BoldtoolStripButton.Checked = false;

                if (isitalicchecked == true)
                    ItalictoolStripButton.Checked = true;
                else
                    ItalictoolStripButton.Checked = false;

                if (isunderlinechecked == true)
                    UnderlinetoolStripButton.Checked = true;
                else
                    UnderlinetoolStripButton.Checked = false;

                if (isstrikeoutchecked == true)
                    StrikethroughtoolStripButton.Checked = true;
                else
                    StrikethroughtoolStripButton.Checked = false;

            }
        }

        private void fontsizetoolStripComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != 13 )
                e.Handled = true;
        }

        private void fontsizetoolStripComboBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (fontsizetoolStripComboBox.Text == "")
                {
                    fontsizetoolStripComboBox.Text = 12.ToString();
                }                                  
                if (int.Parse(fontsizetoolStripComboBox.Text) < 8)
                {
                    fontsizetoolStripComboBox.Text = 8.ToString();
                }

                mainrichTextBox.SelectionFont = new System.Drawing.Font(mainrichTextBox.Font.Name, int.Parse(fontsizetoolStripComboBox.Text));
                mainrichTextBox.Focus();
                mainrichTextBox.SelectionStart = mainrichTextBox.Text.Length;
            }
        }

        private void fontsizetoolStripComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (fontsizetoolStripComboBox.Text.Length == 2)
            {
                e.SuppressKeyPress = !(e.KeyCode == Keys.Back || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Down || e.KeyCode == Keys.Up);
            }
            if (fontsizetoolStripComboBox.SelectionLength > 0)
            {
                e.SuppressKeyPress = !(e.KeyCode == Keys.Back || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Down || e.KeyCode == Keys.Up || e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.NumPad5 || e.KeyCode == Keys.NumPad6 || e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.NumPad8 || e.KeyCode == Keys.NumPad9 || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.D3 || e.KeyCode == Keys.D4 || e.KeyCode == Keys.D5 || e.KeyCode == Keys.D6 || e.KeyCode == Keys.D7 || e.KeyCode == Keys.D8 || e.KeyCode == Keys.D9);
            }
            if (fontsizetoolStripComboBox.Text == 7.ToString())
            {
                e.SuppressKeyPress = !(e.KeyCode == Keys.Back || e.KeyCode == Keys.NumPad0 || e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.D0 || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.Down || e.KeyCode == Keys.Up);
            }
            if (fontsizetoolStripComboBox.Text == 8.ToString())
            {
                e.SuppressKeyPress = !(e.KeyCode == Keys.Back || e.KeyCode == Keys.Down || e.KeyCode == Keys.Up);
            }
            if (fontsizetoolStripComboBox.Text == 9.ToString())
            {
                e.SuppressKeyPress = !(e.KeyCode == Keys.Back || e.KeyCode == Keys.Down || e.KeyCode == Keys.Up);
            }
            if (fontsizetoolStripComboBox.SelectionLength > 0 && fontsizetoolStripComboBox.Text == 8.ToString())
            {
                e.SuppressKeyPress = !(e.KeyCode == Keys.Back || e.KeyCode == Keys.Down || e.KeyCode == Keys.Up || e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.NumPad5 || e.KeyCode == Keys.NumPad6 || e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.NumPad8 || e.KeyCode == Keys.NumPad9 || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.D3 || e.KeyCode == Keys.D4 || e.KeyCode == Keys.D5 || e.KeyCode == Keys.D6 || e.KeyCode == Keys.D7 || e.KeyCode == Keys.D8 || e.KeyCode == Keys.D9);
            }
            if (fontsizetoolStripComboBox.SelectionLength > 0 && fontsizetoolStripComboBox.Text == 9.ToString())
            {
                e.SuppressKeyPress = !(e.KeyCode == Keys.Back || e.KeyCode == Keys.Down || e.KeyCode == Keys.Up || e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.NumPad5 || e.KeyCode == Keys.NumPad6 || e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.NumPad8 || e.KeyCode == Keys.NumPad9 || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.D3 || e.KeyCode == Keys.D4 || e.KeyCode == Keys.D5 || e.KeyCode == Keys.D6 || e.KeyCode == Keys.D7 || e.KeyCode == Keys.D8 || e.KeyCode == Keys.D9);
            }
            if (fontsizetoolStripComboBox.SelectionLength > 0 && fontsizetoolStripComboBox.Text == 7.ToString())
            {
                e.SuppressKeyPress = !(e.KeyCode == Keys.Back || e.KeyCode == Keys.Down || e.KeyCode == Keys.Up || e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.NumPad5 || e.KeyCode == Keys.NumPad6 || e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.NumPad8 || e.KeyCode == Keys.NumPad9 || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.D3 || e.KeyCode == Keys.D4 || e.KeyCode == Keys.D5 || e.KeyCode == Keys.D6 || e.KeyCode == Keys.D7 || e.KeyCode == Keys.D8 || e.KeyCode == Keys.D9);
            }
        }

        private void fontsizetoolStripComboBox_TextUpdate(object sender, EventArgs e)
        {
            isfontsizeselected = true;

            if (mainrichTextBox.SelectionLength > 0)
            {
                if (int.Parse(fontsizetoolStripComboBox.Text) > 7)
                    mainrichTextBox.SelectionFont = new System.Drawing.Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text));
            }
        }

        private void fontsizetoolStripComboBox_TextChanged(object sender, EventArgs e)
        {
            isfontsizeselected = true;

            if (mainrichTextBox.SelectionLength > 0)
            {
                if (int.Parse(fontsizetoolStripComboBox.Text) > 7)
                    mainrichTextBox.SelectionFont = new System.Drawing.Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text));
            }
        }

        private void fontnametoolStripComboBox_TextChanged(object sender, EventArgs e)
        {
            isfontnameselected = true;

            if (mainrichTextBox.SelectionLength > 0)
            {
                 mainrichTextBox.SelectionFont = new System.Drawing.Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text));
            }
        }

        private void fontnametoolStripComboBox_TextUpdate(object sender, EventArgs e)
        {
            isfontnameselected = true;

            if (mainrichTextBox.SelectionLength > 0)
            {
                mainrichTextBox.SelectionFont = new System.Drawing.Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text));
            }
        }
       
        private void fontnametoolStripComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != 13)
                e.Handled = true;
        }

        private void fontnametoolStripComboBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (fontnametoolStripComboBox.Text == "")
                {
                    fontnametoolStripComboBox.Text = "Microsoft Sans Serif";
                }

                mainrichTextBox.Focus();
                mainrichTextBox.SelectionStart = mainrichTextBox.Text.Length;
                mainrichTextBox.SelectionFont = new System.Drawing.Font(fontnametoolStripComboBox.Text, int.Parse(fontsizetoolStripComboBox.Text));
            }
        }

        private void cancelbtn_Click(object sender, EventArgs e)
        {
            colorpanel.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            fontpanel.Visible = false;
        }

        private void VoicepadForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isfilealreadysaved == true)
            {
                if (isfiledirty == true)
                {
                    DialogResult ruslt = MessageBox.Show("Would you like to save your data?", "Ask!",
                                                          MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    if (ruslt == DialogResult.Yes)
                    {
                        if (Path.GetExtension(currentopenfilename) == ".txt")
                            mainrichTextBox.SaveFile(currentopenfilename, RichTextBoxStreamType.PlainText);

                        if (Path.GetExtension(currentopenfilename) == ".rtf")
                            mainrichTextBox.SaveFile(currentopenfilename, RichTextBoxStreamType.RichText);

                        isfiledirty = false;
                        SpeakAsyncSaveChecks();
                    }

                    else if (ruslt == DialogResult.Cancel)
                        e.Cancel = true;

                }
            }

            else
            {
                if (isfiledirty == true)
                {               
                        DialogResult rslt = MessageBox.Show("Would you like to save your data?", "Ask!",
                                                             MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                        if (rslt == DialogResult.Yes)
                        {
                            SpeakAsyncSaveAsChecks();
                            SaveAsFileMenu();
                            if (isfilealreadysaved == true)
                            { Application.Exit(); }
                        }

                        else if (rslt == DialogResult.Cancel)
                            e.Cancel = true;
                    
                }
            }
        }

        private void CutCopyDelItems(bool enabled)
        {
            cuttoolStripButton.Enabled = enabled;
            copytoolStripButton.Enabled = enabled;
            deltoolStripButton.Enabled = enabled;
            cutToolStripMenuItem.Enabled = enabled;
            copyToolStripMenuItem.Enabled = enabled;
            deleteToolStripMenuItem.Enabled = enabled;
            cutToolStripMenuItem1.Enabled = enabled;
            copyToolStripMenuItem1.Enabled = enabled;
            deleteToolStripMenuItem1.Enabled = enabled;
        }

        private void mainrichTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            mainrichTextBox_Click(sender, e);
            if (e.KeyCode == Keys.Tab)
                menuStrip1.Focus();
        }

        private void mainrichTextBox_DoubleClick(object sender, EventArgs e)
        {
            mainrichTextBox_Click(sender, e);
        }

    }
}
