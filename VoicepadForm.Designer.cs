﻿namespace latest_voicepad
{
    partial class VoicepadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VoicepadForm));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.undoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dateTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formatFontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeTextColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePageColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.normalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.boldtoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.italicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.underlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.strikethroughToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.voiceControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.voiceCommandsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enabledToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disabledToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutVoicepadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.MessagetoolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.CapstoolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.colorpanel = new System.Windows.Forms.Panel();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.applybtn = new System.Windows.Forms.Button();
            this.cancelbtn = new System.Windows.Forms.Button();
            this.greylabel = new System.Windows.Forms.Label();
            this.purplelabel = new System.Windows.Forms.Label();
            this.bluelabel = new System.Windows.Forms.Label();
            this.indigolabel = new System.Windows.Forms.Label();
            this.brownlabel = new System.Windows.Forms.Label();
            this.orangelabel = new System.Windows.Forms.Label();
            this.pinklabel = new System.Windows.Forms.Label();
            this.yellowlabel = new System.Windows.Forms.Label();
            this.maroonlabel = new System.Windows.Forms.Label();
            this.lightgreenlabel = new System.Windows.Forms.Label();
            this.greenlabel = new System.Windows.Forms.Label();
            this.redlabel = new System.Windows.Forms.Label();
            this.lightbluelabel = new System.Windows.Forms.Label();
            this.whitelabel = new System.Windows.Forms.Label();
            this.blacklabel = new System.Windows.Forms.Label();
            this.fontpanel = new System.Windows.Forms.Panel();
            this.label94 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label64 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.mainrichTextBox = new System.Windows.Forms.RichTextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.UndotoolStripButton = new System.Windows.Forms.ToolStripButton();
            this.RedotoolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cuttoolStripButton = new System.Windows.Forms.ToolStripButton();
            this.copytoolStripButton = new System.Windows.Forms.ToolStripButton();
            this.PastetoolStripButton = new System.Windows.Forms.ToolStripButton();
            this.deltoolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.BoldtoolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ItalictoolStripButton = new System.Windows.Forms.ToolStripButton();
            this.UnderlinetoolStripButton = new System.Windows.Forms.ToolStripButton();
            this.StrikethroughtoolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.fontnametoolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.fontsizetoolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton15 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton14 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton16 = new System.Windows.Forms.ToolStripButton();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.contextMenuStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.colorpanel.SuspendLayout();
            this.fontpanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem1,
            this.redoToolStripMenuItem1,
            this.selectAllToolStripMenuItem1,
            this.toolStripMenuItem8,
            this.cutToolStripMenuItem1,
            this.copyToolStripMenuItem1,
            this.pasteToolStripMenuItem1,
            this.toolStripMenuItem9,
            this.deleteToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(123, 170);
            // 
            // undoToolStripMenuItem1
            // 
            this.undoToolStripMenuItem1.Enabled = false;
            this.undoToolStripMenuItem1.Image = global::latest_voicepad.Properties.Resources._1484224300_icon_ios7_undo;
            this.undoToolStripMenuItem1.Name = "undoToolStripMenuItem1";
            this.undoToolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
            this.undoToolStripMenuItem1.Text = "&Undo";
            this.undoToolStripMenuItem1.Click += new System.EventHandler(this.undoToolStripMenuItem1_Click);
            // 
            // redoToolStripMenuItem1
            // 
            this.redoToolStripMenuItem1.Enabled = false;
            this.redoToolStripMenuItem1.Image = global::latest_voicepad.Properties.Resources._1484224073_icon_ios7_redo;
            this.redoToolStripMenuItem1.Name = "redoToolStripMenuItem1";
            this.redoToolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
            this.redoToolStripMenuItem1.Text = "&Redo";
            this.redoToolStripMenuItem1.Click += new System.EventHandler(this.redoToolStripMenuItem1_Click);
            // 
            // selectAllToolStripMenuItem1
            // 
            this.selectAllToolStripMenuItem1.Image = global::latest_voicepad.Properties.Resources._1484228659_select_all;
            this.selectAllToolStripMenuItem1.Name = "selectAllToolStripMenuItem1";
            this.selectAllToolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
            this.selectAllToolStripMenuItem1.Text = "&Select All";
            this.selectAllToolStripMenuItem1.Click += new System.EventHandler(this.selectAllToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(119, 6);
            // 
            // cutToolStripMenuItem1
            // 
            this.cutToolStripMenuItem1.Enabled = false;
            this.cutToolStripMenuItem1.Image = global::latest_voicepad.Properties.Resources._1484514405_Cut;
            this.cutToolStripMenuItem1.Name = "cutToolStripMenuItem1";
            this.cutToolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
            this.cutToolStripMenuItem1.Text = "&Cut";
            this.cutToolStripMenuItem1.Click += new System.EventHandler(this.cutToolStripMenuItem1_Click);
            // 
            // copyToolStripMenuItem1
            // 
            this.copyToolStripMenuItem1.Enabled = false;
            this.copyToolStripMenuItem1.Image = global::latest_voicepad.Properties.Resources._1484514506_Copy;
            this.copyToolStripMenuItem1.Name = "copyToolStripMenuItem1";
            this.copyToolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
            this.copyToolStripMenuItem1.Text = "&Copy";
            this.copyToolStripMenuItem1.Click += new System.EventHandler(this.copyToolStripMenuItem1_Click);
            // 
            // pasteToolStripMenuItem1
            // 
            this.pasteToolStripMenuItem1.Image = global::latest_voicepad.Properties.Resources._1484514537_Paste;
            this.pasteToolStripMenuItem1.Name = "pasteToolStripMenuItem1";
            this.pasteToolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
            this.pasteToolStripMenuItem1.Text = "&Paste";
            this.pasteToolStripMenuItem1.Click += new System.EventHandler(this.pasteToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(119, 6);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Enabled = false;
            this.deleteToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484518376_edit_delete;
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.deleteToolStripMenuItem.Text = "&Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.formatToolStripMenuItem,
            this.voiceControlToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(699, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.CheckOnClick = true;
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.toolStripMenuItem1,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripMenuItem2,
            this.printToolStripMenuItem,
            this.printPreviewToolStripMenuItem,
            this.toolStripMenuItem3,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            this.fileToolStripMenuItem.ToolTipText = "File Menu";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484225270_file_add;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.newToolStripMenuItem.Text = "&New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(177, 6);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484224721_open_file;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484224610_save;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484224538_Save_as;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveAsToolStripMenuItem.Text = "&Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(177, 6);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484223749_vector_66_15;
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.printToolStripMenuItem.Text = "&Print";
            // 
            // printPreviewToolStripMenuItem
            // 
            this.printPreviewToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484228290_document_print_preview;
            this.printPreviewToolStripMenuItem.Name = "printPreviewToolStripMenuItem";
            this.printPreviewToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.P)));
            this.printPreviewToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.printPreviewToolStripMenuItem.Text = "Print &Preview";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(177, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484225420_close;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.CheckOnClick = true;
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripMenuItem4,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.deleteToolStripMenuItem1,
            this.toolStripMenuItem6,
            this.selectAllToolStripMenuItem,
            this.dateTimeToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            this.editToolStripMenuItem.ToolTipText = "Edit Menu";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Enabled = false;
            this.undoToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484224300_icon_ios7_undo;
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.undoToolStripMenuItem.Text = "&Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Enabled = false;
            this.redoToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484224073_icon_ios7_redo;
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.redoToolStripMenuItem.Text = "&Redo";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(161, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Enabled = false;
            this.cutToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484514405_Cut;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.cutToolStripMenuItem.Text = "&Cut";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Enabled = false;
            this.copyToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484514506_Copy;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.copyToolStripMenuItem.Text = "&Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484514537_Paste;
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.pasteToolStripMenuItem.Text = "&Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem1
            // 
            this.deleteToolStripMenuItem1.Enabled = false;
            this.deleteToolStripMenuItem1.Image = global::latest_voicepad.Properties.Resources._1484518376_edit_delete;
            this.deleteToolStripMenuItem1.Name = "deleteToolStripMenuItem1";
            this.deleteToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.deleteToolStripMenuItem1.Size = new System.Drawing.Size(164, 22);
            this.deleteToolStripMenuItem1.Text = "&Delete";
            this.deleteToolStripMenuItem1.Click += new System.EventHandler(this.deleteToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(161, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484228659_select_all;
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.selectAllToolStripMenuItem.Text = "&Select All";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // dateTimeToolStripMenuItem
            // 
            this.dateTimeToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484240506_file_temporary;
            this.dateTimeToolStripMenuItem.Name = "dateTimeToolStripMenuItem";
            this.dateTimeToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.dateTimeToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.dateTimeToolStripMenuItem.Text = "Date/Time";
            this.dateTimeToolStripMenuItem.Click += new System.EventHandler(this.dateTimeToolStripMenuItem_Click);
            // 
            // formatToolStripMenuItem
            // 
            this.formatToolStripMenuItem.CheckOnClick = true;
            this.formatToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.formatFontToolStripMenuItem,
            this.changeTextColorToolStripMenuItem,
            this.changePageColorToolStripMenuItem,
            this.toolStripMenuItem5,
            this.normalToolStripMenuItem,
            this.boldtoolStripMenuItem,
            this.italicToolStripMenuItem,
            this.underlineToolStripMenuItem,
            this.strikethroughToolStripMenuItem});
            this.formatToolStripMenuItem.Name = "formatToolStripMenuItem";
            this.formatToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.formatToolStripMenuItem.Text = "F&ormat";
            this.formatToolStripMenuItem.ToolTipText = "Text Format Menu";
            // 
            // formatFontToolStripMenuItem
            // 
            this.formatFontToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484242285_preferences_desktop_font;
            this.formatFontToolStripMenuItem.Name = "formatFontToolStripMenuItem";
            this.formatFontToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.formatFontToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.formatFontToolStripMenuItem.Text = "&Text Format";
            this.formatFontToolStripMenuItem.Click += new System.EventHandler(this.formatFontToolStripMenuItem_Click);
            // 
            // changeTextColorToolStripMenuItem
            // 
            this.changeTextColorToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources.Oxygen_Icons_org_Oxygen_Actions_format_stroke_color;
            this.changeTextColorToolStripMenuItem.Name = "changeTextColorToolStripMenuItem";
            this.changeTextColorToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.changeTextColorToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.changeTextColorToolStripMenuItem.Text = "&Change Text Color";
            this.changeTextColorToolStripMenuItem.Click += new System.EventHandler(this.changeTextColorToolStripMenuItem_Click);
            // 
            // changePageColorToolStripMenuItem
            // 
            this.changePageColorToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources._1484240754_preferences_desktop_color;
            this.changePageColorToolStripMenuItem.Name = "changePageColorToolStripMenuItem";
            this.changePageColorToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.changePageColorToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.changePageColorToolStripMenuItem.Text = "&Change Page Color";
            this.changePageColorToolStripMenuItem.Click += new System.EventHandler(this.changePageColorToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(192, 6);
            // 
            // normalToolStripMenuItem
            // 
            this.normalToolStripMenuItem.Name = "normalToolStripMenuItem";
            this.normalToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.normalToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.normalToolStripMenuItem.Text = "&Normal";
            this.normalToolStripMenuItem.Click += new System.EventHandler(this.normalToolStripMenuItem_Click);
            // 
            // boldtoolStripMenuItem
            // 
            this.boldtoolStripMenuItem.CheckOnClick = true;
            this.boldtoolStripMenuItem.Image = global::latest_voicepad.Properties.Resources.Bold_52px1;
            this.boldtoolStripMenuItem.Name = "boldtoolStripMenuItem";
            this.boldtoolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this.boldtoolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.boldtoolStripMenuItem.Text = "&Bold";
            this.boldtoolStripMenuItem.Click += new System.EventHandler(this.boldtoolStripMenuItem_Click);
            // 
            // italicToolStripMenuItem
            // 
            this.italicToolStripMenuItem.CheckOnClick = true;
            this.italicToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources.Italic_52px1;
            this.italicToolStripMenuItem.Name = "italicToolStripMenuItem";
            this.italicToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.italicToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.italicToolStripMenuItem.Text = "&Italic";
            this.italicToolStripMenuItem.Click += new System.EventHandler(this.italicToolStripMenuItem_Click);
            // 
            // underlineToolStripMenuItem
            // 
            this.underlineToolStripMenuItem.CheckOnClick = true;
            this.underlineToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources.Underline_52px1;
            this.underlineToolStripMenuItem.Name = "underlineToolStripMenuItem";
            this.underlineToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U)));
            this.underlineToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.underlineToolStripMenuItem.Text = "&Underline";
            this.underlineToolStripMenuItem.Click += new System.EventHandler(this.underlineToolStripMenuItem_Click);
            // 
            // strikethroughToolStripMenuItem
            // 
            this.strikethroughToolStripMenuItem.CheckOnClick = true;
            this.strikethroughToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources.Strikethrough_52px1;
            this.strikethroughToolStripMenuItem.Name = "strikethroughToolStripMenuItem";
            this.strikethroughToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.strikethroughToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.strikethroughToolStripMenuItem.Text = "&Strikethrough";
            this.strikethroughToolStripMenuItem.Click += new System.EventHandler(this.strikethroughToolStripMenuItem_Click);
            // 
            // voiceControlToolStripMenuItem
            // 
            this.voiceControlToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.voiceCommandsToolStripMenuItem});
            this.voiceControlToolStripMenuItem.Name = "voiceControlToolStripMenuItem";
            this.voiceControlToolStripMenuItem.Size = new System.Drawing.Size(126, 20);
            this.voiceControlToolStripMenuItem.Text = "V&oice Recognization";
            // 
            // voiceCommandsToolStripMenuItem
            // 
            this.voiceCommandsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enabledToolStripMenuItem,
            this.disabledToolStripMenuItem});
            this.voiceCommandsToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources.Voice_Recognition_Scan_16px1;
            this.voiceCommandsToolStripMenuItem.Name = "voiceCommandsToolStripMenuItem";
            this.voiceCommandsToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.voiceCommandsToolStripMenuItem.Text = "&Voice Commands";
            // 
            // enabledToolStripMenuItem
            // 
            this.enabledToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources.Microphone_16px;
            this.enabledToolStripMenuItem.Name = "enabledToolStripMenuItem";
            this.enabledToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.enabledToolStripMenuItem.Text = "&Enabled";
            this.enabledToolStripMenuItem.Click += new System.EventHandler(this.enabledToolStripMenuItem_Click);
            // 
            // disabledToolStripMenuItem
            // 
            this.disabledToolStripMenuItem.Enabled = false;
            this.disabledToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources.No_Microphone_16px;
            this.disabledToolStripMenuItem.Name = "disabledToolStripMenuItem";
            this.disabledToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.disabledToolStripMenuItem.Text = "&Disabled";
            this.disabledToolStripMenuItem.Click += new System.EventHandler(this.disabledToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.CheckOnClick = true;
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutVoicepadToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            this.helpToolStripMenuItem.ToolTipText = "Help Menu";
            // 
            // aboutVoicepadToolStripMenuItem
            // 
            this.aboutVoicepadToolStripMenuItem.Image = global::latest_voicepad.Properties.Resources.Oxygen_Icons_org_Oxygen_Actions_help_about;
            this.aboutVoicepadToolStripMenuItem.Name = "aboutVoicepadToolStripMenuItem";
            this.aboutVoicepadToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.aboutVoicepadToolStripMenuItem.Text = "&About Voicepad";
            this.aboutVoicepadToolStripMenuItem.Click += new System.EventHandler(this.aboutVoicepadToolStripMenuItem_Click);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.BackColor = System.Drawing.Color.CadetBlue;
            this.toolStripContainer1.ContentPanel.Controls.Add(this.colorpanel);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.fontpanel);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.panel1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(699, 703);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.LeftToolStripPanelVisible = false;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 24);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.RightToolStripPanelVisible = false;
            this.toolStripContainer1.Size = new System.Drawing.Size(699, 752);
            this.toolStripContainer1.TabIndex = 2;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MessagetoolStripStatusLabel,
            this.toolStripStatusLabel1,
            this.CapstoolStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(699, 24);
            this.statusStrip1.TabIndex = 0;
            // 
            // MessagetoolStripStatusLabel
            // 
            this.MessagetoolStripStatusLabel.AutoSize = false;
            this.MessagetoolStripStatusLabel.AutoToolTip = true;
            this.MessagetoolStripStatusLabel.BackColor = System.Drawing.Color.Gainsboro;
            this.MessagetoolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.MessagetoolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken;
            this.MessagetoolStripStatusLabel.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MessagetoolStripStatusLabel.ForeColor = System.Drawing.Color.Black;
            this.MessagetoolStripStatusLabel.Name = "MessagetoolStripStatusLabel";
            this.MessagetoolStripStatusLabel.Size = new System.Drawing.Size(220, 19);
            this.MessagetoolStripStatusLabel.ToolTipText = "Action Description";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(18, 19);
            this.toolStripStatusLabel1.Text = " - ";
            // 
            // CapstoolStripStatusLabel
            // 
            this.CapstoolStripStatusLabel.AutoSize = false;
            this.CapstoolStripStatusLabel.BackColor = System.Drawing.Color.Silver;
            this.CapstoolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.CapstoolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken;
            this.CapstoolStripStatusLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CapstoolStripStatusLabel.Name = "CapstoolStripStatusLabel";
            this.CapstoolStripStatusLabel.Size = new System.Drawing.Size(75, 19);
            this.CapstoolStripStatusLabel.Text = "Caps OFF";
            // 
            // colorpanel
            // 
            this.colorpanel.BackColor = System.Drawing.Color.FloralWhite;
            this.colorpanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.colorpanel.Controls.Add(this.label48);
            this.colorpanel.Controls.Add(this.label47);
            this.colorpanel.Controls.Add(this.label40);
            this.colorpanel.Controls.Add(this.label46);
            this.colorpanel.Controls.Add(this.label35);
            this.colorpanel.Controls.Add(this.label45);
            this.colorpanel.Controls.Add(this.label39);
            this.colorpanel.Controls.Add(this.label44);
            this.colorpanel.Controls.Add(this.label34);
            this.colorpanel.Controls.Add(this.label43);
            this.colorpanel.Controls.Add(this.label38);
            this.colorpanel.Controls.Add(this.label42);
            this.colorpanel.Controls.Add(this.label37);
            this.colorpanel.Controls.Add(this.label33);
            this.colorpanel.Controls.Add(this.label41);
            this.colorpanel.Controls.Add(this.label36);
            this.colorpanel.Controls.Add(this.label32);
            this.colorpanel.Controls.Add(this.label31);
            this.colorpanel.Controls.Add(this.label30);
            this.colorpanel.Controls.Add(this.label25);
            this.colorpanel.Controls.Add(this.label20);
            this.colorpanel.Controls.Add(this.label29);
            this.colorpanel.Controls.Add(this.label24);
            this.colorpanel.Controls.Add(this.label19);
            this.colorpanel.Controls.Add(this.label28);
            this.colorpanel.Controls.Add(this.label27);
            this.colorpanel.Controls.Add(this.label23);
            this.colorpanel.Controls.Add(this.label22);
            this.colorpanel.Controls.Add(this.label26);
            this.colorpanel.Controls.Add(this.label18);
            this.colorpanel.Controls.Add(this.label21);
            this.colorpanel.Controls.Add(this.label17);
            this.colorpanel.Controls.Add(this.label16);
            this.colorpanel.Controls.Add(this.applybtn);
            this.colorpanel.Controls.Add(this.cancelbtn);
            this.colorpanel.Controls.Add(this.greylabel);
            this.colorpanel.Controls.Add(this.purplelabel);
            this.colorpanel.Controls.Add(this.bluelabel);
            this.colorpanel.Controls.Add(this.indigolabel);
            this.colorpanel.Controls.Add(this.brownlabel);
            this.colorpanel.Controls.Add(this.orangelabel);
            this.colorpanel.Controls.Add(this.pinklabel);
            this.colorpanel.Controls.Add(this.yellowlabel);
            this.colorpanel.Controls.Add(this.maroonlabel);
            this.colorpanel.Controls.Add(this.lightgreenlabel);
            this.colorpanel.Controls.Add(this.greenlabel);
            this.colorpanel.Controls.Add(this.redlabel);
            this.colorpanel.Controls.Add(this.lightbluelabel);
            this.colorpanel.Controls.Add(this.whitelabel);
            this.colorpanel.Controls.Add(this.blacklabel);
            this.colorpanel.Location = new System.Drawing.Point(219, 96);
            this.colorpanel.Name = "colorpanel";
            this.colorpanel.Size = new System.Drawing.Size(260, 510);
            this.colorpanel.TabIndex = 4;
            this.colorpanel.Visible = false;
            // 
            // label48
            // 
            this.label48.Font = new System.Drawing.Font("Arial Narrow", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.label48.Location = new System.Drawing.Point(34, 16);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(182, 42);
            this.label48.TabIndex = 54;
            this.label48.Text = "Decide Color You Want and Just Say Its Number:";
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.Color.Gray;
            this.label47.Location = new System.Drawing.Point(171, 415);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(56, 15);
            this.label47.TabIndex = 37;
            // 
            // label40
            // 
            this.label40.BackColor = System.Drawing.Color.Yellow;
            this.label40.Location = new System.Drawing.Point(171, 295);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(56, 15);
            this.label40.TabIndex = 38;
            // 
            // label46
            // 
            this.label46.Location = new System.Drawing.Point(171, 298);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(56, 15);
            this.label46.TabIndex = 39;
            // 
            // label35
            // 
            this.label35.BackColor = System.Drawing.Color.Pink;
            this.label35.Location = new System.Drawing.Point(171, 178);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(56, 15);
            this.label35.TabIndex = 40;
            // 
            // label45
            // 
            this.label45.BackColor = System.Drawing.Color.Indigo;
            this.label45.Location = new System.Drawing.Point(171, 391);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(56, 15);
            this.label45.TabIndex = 41;
            // 
            // label39
            // 
            this.label39.BackColor = System.Drawing.Color.LightGreen;
            this.label39.Location = new System.Drawing.Point(171, 272);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(56, 15);
            this.label39.TabIndex = 42;
            // 
            // label44
            // 
            this.label44.Location = new System.Drawing.Point(171, 274);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(56, 15);
            this.label44.TabIndex = 43;
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.Maroon;
            this.label34.Location = new System.Drawing.Point(171, 155);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(56, 15);
            this.label34.TabIndex = 44;
            // 
            // label43
            // 
            this.label43.BackColor = System.Drawing.Color.OrangeRed;
            this.label43.Location = new System.Drawing.Point(171, 367);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(56, 15);
            this.label43.TabIndex = 53;
            // 
            // label38
            // 
            this.label38.BackColor = System.Drawing.Color.Green;
            this.label38.Location = new System.Drawing.Point(171, 248);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(56, 15);
            this.label38.TabIndex = 46;
            // 
            // label42
            // 
            this.label42.BackColor = System.Drawing.Color.Purple;
            this.label42.Location = new System.Drawing.Point(171, 343);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(56, 15);
            this.label42.TabIndex = 45;
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.Color.LightBlue;
            this.label37.Location = new System.Drawing.Point(171, 224);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(56, 15);
            this.label37.TabIndex = 47;
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.Color.Red;
            this.label33.Location = new System.Drawing.Point(171, 131);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(56, 15);
            this.label33.TabIndex = 48;
            // 
            // label41
            // 
            this.label41.BackColor = System.Drawing.Color.Brown;
            this.label41.Location = new System.Drawing.Point(171, 319);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(56, 15);
            this.label41.TabIndex = 49;
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.Color.Blue;
            this.label36.Location = new System.Drawing.Point(171, 200);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(56, 15);
            this.label36.TabIndex = 50;
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.Color.White;
            this.label32.Location = new System.Drawing.Point(171, 107);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(56, 15);
            this.label32.TabIndex = 51;
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(171, 83);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(56, 15);
            this.label31.TabIndex = 52;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(75, 413);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(32, 16);
            this.label30.TabIndex = 35;
            this.label30.Text = "Grey";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(75, 295);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(42, 16);
            this.label25.TabIndex = 34;
            this.label25.Text = "Yellow";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(75, 177);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(31, 16);
            this.label20.TabIndex = 33;
            this.label20.Text = "Pink";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(75, 389);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(43, 16);
            this.label29.TabIndex = 32;
            this.label29.Text = "Indigo";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(75, 271);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(70, 16);
            this.label24.TabIndex = 31;
            this.label24.Text = "Light Green";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(75, 153);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(48, 16);
            this.label19.TabIndex = 36;
            this.label19.Text = "Maroon";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(75, 365);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(46, 16);
            this.label28.TabIndex = 30;
            this.label28.Text = "Orange";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(75, 341);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(42, 16);
            this.label27.TabIndex = 22;
            this.label27.Text = "Purple";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(75, 247);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(39, 16);
            this.label23.TabIndex = 28;
            this.label23.Text = "Green";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(75, 223);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(63, 16);
            this.label22.TabIndex = 27;
            this.label22.Text = "Light Blue";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(75, 317);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(42, 16);
            this.label26.TabIndex = 26;
            this.label26.Text = "Brown";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(75, 129);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 16);
            this.label18.TabIndex = 25;
            this.label18.Text = "Red";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(75, 199);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(32, 16);
            this.label21.TabIndex = 24;
            this.label21.Text = "Blue";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(75, 105);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(39, 16);
            this.label17.TabIndex = 23;
            this.label17.Text = "White";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(75, 81);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 16);
            this.label16.TabIndex = 29;
            this.label16.Text = "Black";
            // 
            // applybtn
            // 
            this.applybtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.applybtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.applybtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.applybtn.Location = new System.Drawing.Point(49, 468);
            this.applybtn.Name = "applybtn";
            this.applybtn.Size = new System.Drawing.Size(75, 23);
            this.applybtn.TabIndex = 21;
            this.applybtn.Text = "Apply";
            this.applybtn.UseVisualStyleBackColor = false;
            // 
            // cancelbtn
            // 
            this.cancelbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cancelbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelbtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.cancelbtn.Location = new System.Drawing.Point(142, 468);
            this.cancelbtn.Name = "cancelbtn";
            this.cancelbtn.Size = new System.Drawing.Size(75, 23);
            this.cancelbtn.TabIndex = 20;
            this.cancelbtn.Text = "Cancel";
            this.cancelbtn.UseVisualStyleBackColor = false;
            // 
            // greylabel
            // 
            this.greylabel.BackColor = System.Drawing.Color.White;
            this.greylabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.greylabel.Location = new System.Drawing.Point(35, 412);
            this.greylabel.Name = "greylabel";
            this.greylabel.Size = new System.Drawing.Size(23, 19);
            this.greylabel.TabIndex = 6;
            this.greylabel.Text = "15.";
            // 
            // purplelabel
            // 
            this.purplelabel.BackColor = System.Drawing.Color.White;
            this.purplelabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.purplelabel.Location = new System.Drawing.Point(35, 342);
            this.purplelabel.Name = "purplelabel";
            this.purplelabel.Size = new System.Drawing.Size(23, 19);
            this.purplelabel.TabIndex = 12;
            this.purplelabel.Text = "12.";
            // 
            // bluelabel
            // 
            this.bluelabel.BackColor = System.Drawing.Color.White;
            this.bluelabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bluelabel.Location = new System.Drawing.Point(35, 200);
            this.bluelabel.Name = "bluelabel";
            this.bluelabel.Size = new System.Drawing.Size(19, 18);
            this.bluelabel.TabIndex = 7;
            this.bluelabel.Text = "6.";
            // 
            // indigolabel
            // 
            this.indigolabel.BackColor = System.Drawing.Color.White;
            this.indigolabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.indigolabel.Location = new System.Drawing.Point(35, 389);
            this.indigolabel.Name = "indigolabel";
            this.indigolabel.Size = new System.Drawing.Size(23, 19);
            this.indigolabel.TabIndex = 8;
            this.indigolabel.Text = "14.";
            // 
            // brownlabel
            // 
            this.brownlabel.BackColor = System.Drawing.Color.White;
            this.brownlabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brownlabel.Location = new System.Drawing.Point(35, 319);
            this.brownlabel.Name = "brownlabel";
            this.brownlabel.Size = new System.Drawing.Size(23, 19);
            this.brownlabel.TabIndex = 9;
            this.brownlabel.Text = "11.";
            // 
            // orangelabel
            // 
            this.orangelabel.BackColor = System.Drawing.Color.White;
            this.orangelabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orangelabel.Location = new System.Drawing.Point(35, 365);
            this.orangelabel.Name = "orangelabel";
            this.orangelabel.Size = new System.Drawing.Size(23, 19);
            this.orangelabel.TabIndex = 10;
            this.orangelabel.Text = "13.";
            // 
            // pinklabel
            // 
            this.pinklabel.BackColor = System.Drawing.Color.White;
            this.pinklabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pinklabel.Location = new System.Drawing.Point(35, 177);
            this.pinklabel.Name = "pinklabel";
            this.pinklabel.Size = new System.Drawing.Size(19, 18);
            this.pinklabel.TabIndex = 11;
            this.pinklabel.Text = "5.";
            // 
            // yellowlabel
            // 
            this.yellowlabel.BackColor = System.Drawing.Color.White;
            this.yellowlabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yellowlabel.Location = new System.Drawing.Point(35, 295);
            this.yellowlabel.Name = "yellowlabel";
            this.yellowlabel.Size = new System.Drawing.Size(23, 19);
            this.yellowlabel.TabIndex = 14;
            this.yellowlabel.Text = "10.";
            // 
            // maroonlabel
            // 
            this.maroonlabel.BackColor = System.Drawing.Color.White;
            this.maroonlabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maroonlabel.Location = new System.Drawing.Point(35, 153);
            this.maroonlabel.Name = "maroonlabel";
            this.maroonlabel.Size = new System.Drawing.Size(19, 18);
            this.maroonlabel.TabIndex = 13;
            this.maroonlabel.Text = "4.";
            // 
            // lightgreenlabel
            // 
            this.lightgreenlabel.BackColor = System.Drawing.Color.White;
            this.lightgreenlabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lightgreenlabel.Location = new System.Drawing.Point(35, 271);
            this.lightgreenlabel.Name = "lightgreenlabel";
            this.lightgreenlabel.Size = new System.Drawing.Size(19, 18);
            this.lightgreenlabel.TabIndex = 18;
            this.lightgreenlabel.Text = "9.";
            // 
            // greenlabel
            // 
            this.greenlabel.BackColor = System.Drawing.Color.White;
            this.greenlabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.greenlabel.Location = new System.Drawing.Point(35, 247);
            this.greenlabel.Name = "greenlabel";
            this.greenlabel.Size = new System.Drawing.Size(19, 18);
            this.greenlabel.TabIndex = 17;
            this.greenlabel.Text = "8.";
            // 
            // redlabel
            // 
            this.redlabel.BackColor = System.Drawing.Color.White;
            this.redlabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.redlabel.Location = new System.Drawing.Point(35, 129);
            this.redlabel.Name = "redlabel";
            this.redlabel.Size = new System.Drawing.Size(19, 18);
            this.redlabel.TabIndex = 19;
            this.redlabel.Text = "3.";
            // 
            // lightbluelabel
            // 
            this.lightbluelabel.BackColor = System.Drawing.Color.White;
            this.lightbluelabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lightbluelabel.Location = new System.Drawing.Point(35, 224);
            this.lightbluelabel.Name = "lightbluelabel";
            this.lightbluelabel.Size = new System.Drawing.Size(19, 18);
            this.lightbluelabel.TabIndex = 15;
            this.lightbluelabel.Text = "7.";
            // 
            // whitelabel
            // 
            this.whitelabel.BackColor = System.Drawing.Color.White;
            this.whitelabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.whitelabel.Location = new System.Drawing.Point(35, 105);
            this.whitelabel.Name = "whitelabel";
            this.whitelabel.Size = new System.Drawing.Size(19, 18);
            this.whitelabel.TabIndex = 16;
            this.whitelabel.Text = "2.";
            // 
            // blacklabel
            // 
            this.blacklabel.BackColor = System.Drawing.Color.White;
            this.blacklabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.blacklabel.Location = new System.Drawing.Point(35, 82);
            this.blacklabel.Name = "blacklabel";
            this.blacklabel.Size = new System.Drawing.Size(19, 18);
            this.blacklabel.TabIndex = 5;
            this.blacklabel.Text = "1.";
            // 
            // fontpanel
            // 
            this.fontpanel.BackColor = System.Drawing.Color.FloralWhite;
            this.fontpanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fontpanel.Controls.Add(this.label94);
            this.fontpanel.Controls.Add(this.label65);
            this.fontpanel.Controls.Add(this.label93);
            this.fontpanel.Controls.Add(this.label66);
            this.fontpanel.Controls.Add(this.label92);
            this.fontpanel.Controls.Add(this.label91);
            this.fontpanel.Controls.Add(this.label67);
            this.fontpanel.Controls.Add(this.label90);
            this.fontpanel.Controls.Add(this.label68);
            this.fontpanel.Controls.Add(this.label89);
            this.fontpanel.Controls.Add(this.label69);
            this.fontpanel.Controls.Add(this.label88);
            this.fontpanel.Controls.Add(this.label70);
            this.fontpanel.Controls.Add(this.label87);
            this.fontpanel.Controls.Add(this.label71);
            this.fontpanel.Controls.Add(this.label86);
            this.fontpanel.Controls.Add(this.label72);
            this.fontpanel.Controls.Add(this.label85);
            this.fontpanel.Controls.Add(this.label73);
            this.fontpanel.Controls.Add(this.label84);
            this.fontpanel.Controls.Add(this.label74);
            this.fontpanel.Controls.Add(this.label83);
            this.fontpanel.Controls.Add(this.label75);
            this.fontpanel.Controls.Add(this.label82);
            this.fontpanel.Controls.Add(this.label76);
            this.fontpanel.Controls.Add(this.label81);
            this.fontpanel.Controls.Add(this.label77);
            this.fontpanel.Controls.Add(this.label80);
            this.fontpanel.Controls.Add(this.label78);
            this.fontpanel.Controls.Add(this.label79);
            this.fontpanel.Controls.Add(this.button1);
            this.fontpanel.Controls.Add(this.button2);
            this.fontpanel.Controls.Add(this.label64);
            this.fontpanel.Controls.Add(this.label2);
            this.fontpanel.Controls.Add(this.label63);
            this.fontpanel.Controls.Add(this.label3);
            this.fontpanel.Controls.Add(this.label62);
            this.fontpanel.Controls.Add(this.label4);
            this.fontpanel.Controls.Add(this.label61);
            this.fontpanel.Controls.Add(this.label5);
            this.fontpanel.Controls.Add(this.label60);
            this.fontpanel.Controls.Add(this.label6);
            this.fontpanel.Controls.Add(this.label59);
            this.fontpanel.Controls.Add(this.label7);
            this.fontpanel.Controls.Add(this.label58);
            this.fontpanel.Controls.Add(this.label8);
            this.fontpanel.Controls.Add(this.label57);
            this.fontpanel.Controls.Add(this.label9);
            this.fontpanel.Controls.Add(this.label56);
            this.fontpanel.Controls.Add(this.label55);
            this.fontpanel.Controls.Add(this.label10);
            this.fontpanel.Controls.Add(this.label54);
            this.fontpanel.Controls.Add(this.label11);
            this.fontpanel.Controls.Add(this.label53);
            this.fontpanel.Controls.Add(this.label12);
            this.fontpanel.Controls.Add(this.label52);
            this.fontpanel.Controls.Add(this.label13);
            this.fontpanel.Controls.Add(this.label51);
            this.fontpanel.Controls.Add(this.label14);
            this.fontpanel.Controls.Add(this.label50);
            this.fontpanel.Controls.Add(this.label15);
            this.fontpanel.Controls.Add(this.label49);
            this.fontpanel.Controls.Add(this.label1);
            this.fontpanel.Location = new System.Drawing.Point(115, 96);
            this.fontpanel.Name = "fontpanel";
            this.fontpanel.Size = new System.Drawing.Size(469, 510);
            this.fontpanel.TabIndex = 5;
            this.fontpanel.Visible = false;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(284, 408);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(62, 17);
            this.label94.TabIndex = 86;
            this.label94.Text = "Segoe UI";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Broadway", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(62, 409);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(86, 15);
            this.label65.TabIndex = 86;
            this.label65.Text = "Broadway";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("High Tower Text", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(284, 290);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(115, 15);
            this.label93.TabIndex = 85;
            this.label93.Text = "High Tower Text";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Blackadder ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(62, 291);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(98, 18);
            this.label66.TabIndex = 85;
            this.label66.Text = "Blackadder ITC";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Cooper Black", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(284, 172);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(55, 15);
            this.label92.TabIndex = 84;
            this.label92.Text = "Cooper";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(284, 384);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(146, 16);
            this.label91.TabIndex = 83;
            this.label91.Text = "Microsoft Sans Serif";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Baskerville Old Face", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(62, 173);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(135, 14);
            this.label67.TabIndex = 84;
            this.label67.Text = "Baskerville Old Face";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Garamond", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(284, 266);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(65, 14);
            this.label90.TabIndex = 82;
            this.label90.Text = "Garamond";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Bookman Old Style", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(62, 385);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(139, 16);
            this.label68.TabIndex = 83;
            this.label68.Text = "Bookman Old Style";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Chiller", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(284, 148);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(39, 16);
            this.label89.TabIndex = 87;
            this.label89.Text = "Chiller";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Bodoni MT", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(62, 267);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(67, 17);
            this.label69.TabIndex = 82;
            this.label69.Text = "Bodoni MT";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Lucida Calligraphy", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.Black;
            this.label88.Location = new System.Drawing.Point(284, 360);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(160, 17);
            this.label88.TabIndex = 81;
            this.label88.Text = "Lucida Calligraphy";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(62, 149);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(85, 15);
            this.label70.TabIndex = 87;
            this.label70.Text = "Modern No. 20";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(284, 336);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(97, 18);
            this.label87.TabIndex = 73;
            this.label87.Text = "Kristen ITC";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Bradley Hand ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(62, 361);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(124, 16);
            this.label71.TabIndex = 81;
            this.label71.Text = "Bradley Hand ITC";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Forte", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(284, 242);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(40, 14);
            this.label86.TabIndex = 79;
            this.label86.Text = "Forte";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Bookman Old Style", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(62, 337);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(139, 16);
            this.label72.TabIndex = 73;
            this.label72.Text = "Bookman Old Style";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Elephant", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(284, 218);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(74, 17);
            this.label85.TabIndex = 78;
            this.label85.Text = "Elephant";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Berlin Sans FB Demi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(62, 243);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(87, 16);
            this.label73.TabIndex = 79;
            this.label73.Text = "Berlin Sans FB";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Jokerman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(284, 312);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(72, 20);
            this.label84.TabIndex = 77;
            this.label84.Text = "Jokerman";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Bell MT", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(62, 219);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(54, 17);
            this.label74.TabIndex = 78;
            this.label74.Text = "Bell MT";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(284, 124);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(64, 16);
            this.label83.TabIndex = 76;
            this.label83.Text = "Century";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Book Antiqua", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(62, 313);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(91, 18);
            this.label75.TabIndex = 77;
            this.label75.Text = "Book Antiqua";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Curlz MT", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(284, 194);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(63, 17);
            this.label82.TabIndex = 75;
            this.label82.Text = "Curlz MT";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(62, 125);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(31, 16);
            this.label76.TabIndex = 76;
            this.label76.Text = "Arial";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(284, 100);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(63, 15);
            this.label81.TabIndex = 74;
            this.label81.Text = "Consolas";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Bauhaus 93", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(62, 195);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(73, 15);
            this.label77.TabIndex = 75;
            this.label77.Text = "Bauhaus 93";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(284, 76);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(41, 15);
            this.label80.TabIndex = 80;
            this.label80.Text = "Calibri";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Algerian", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(62, 101);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(71, 15);
            this.label78.TabIndex = 74;
            this.label78.Text = "Algerian";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Agency FB", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(62, 77);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(59, 17);
            this.label79.TabIndex = 80;
            this.label79.Text = "Agency FB";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button1.Location = new System.Drawing.Point(138, 462);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 72;
            this.button1.Text = "Apply";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button2.Location = new System.Drawing.Point(254, 462);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 71;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // label64
            // 
            this.label64.BackColor = System.Drawing.Color.White;
            this.label64.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(243, 407);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(23, 19);
            this.label64.TabIndex = 57;
            this.label64.Text = "30.";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(26, 407);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 19);
            this.label2.TabIndex = 57;
            this.label2.Text = "15.";
            // 
            // label63
            // 
            this.label63.BackColor = System.Drawing.Color.White;
            this.label63.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(243, 337);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(23, 19);
            this.label63.TabIndex = 63;
            this.label63.Text = "27.";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(26, 337);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 19);
            this.label3.TabIndex = 63;
            this.label3.Text = "12.";
            // 
            // label62
            // 
            this.label62.BackColor = System.Drawing.Color.White;
            this.label62.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(243, 195);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(23, 19);
            this.label62.TabIndex = 58;
            this.label62.Text = "21.";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(26, 195);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 18);
            this.label4.TabIndex = 58;
            this.label4.Text = "6.";
            // 
            // label61
            // 
            this.label61.BackColor = System.Drawing.Color.White;
            this.label61.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(243, 384);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(23, 19);
            this.label61.TabIndex = 59;
            this.label61.Text = "29.";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(26, 384);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 19);
            this.label5.TabIndex = 59;
            this.label5.Text = "14.";
            // 
            // label60
            // 
            this.label60.BackColor = System.Drawing.Color.White;
            this.label60.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(243, 314);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(23, 19);
            this.label60.TabIndex = 60;
            this.label60.Text = "26.";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(26, 314);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 19);
            this.label6.TabIndex = 60;
            this.label6.Text = "11.";
            // 
            // label59
            // 
            this.label59.BackColor = System.Drawing.Color.White;
            this.label59.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(243, 360);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(23, 19);
            this.label59.TabIndex = 61;
            this.label59.Text = "28.";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(26, 360);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 19);
            this.label7.TabIndex = 61;
            this.label7.Text = "13.";
            // 
            // label58
            // 
            this.label58.BackColor = System.Drawing.Color.White;
            this.label58.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(243, 172);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(23, 19);
            this.label58.TabIndex = 62;
            this.label58.Text = "20.";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(26, 172);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(19, 18);
            this.label8.TabIndex = 62;
            this.label8.Text = "5.";
            // 
            // label57
            // 
            this.label57.BackColor = System.Drawing.Color.White;
            this.label57.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(243, 290);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(23, 19);
            this.label57.TabIndex = 65;
            this.label57.Text = "25.";
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(26, 290);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 19);
            this.label9.TabIndex = 65;
            this.label9.Text = "10.";
            // 
            // label56
            // 
            this.label56.BackColor = System.Drawing.Color.White;
            this.label56.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(243, 148);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(23, 19);
            this.label56.TabIndex = 64;
            this.label56.Text = "19.";
            // 
            // label55
            // 
            this.label55.BackColor = System.Drawing.Color.White;
            this.label55.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(243, 266);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(23, 19);
            this.label55.TabIndex = 69;
            this.label55.Text = "24.";
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.White;
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(26, 148);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 18);
            this.label10.TabIndex = 64;
            this.label10.Text = "4.";
            // 
            // label54
            // 
            this.label54.BackColor = System.Drawing.Color.White;
            this.label54.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(243, 242);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(23, 19);
            this.label54.TabIndex = 68;
            this.label54.Text = "23.";
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.White;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(26, 266);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 18);
            this.label11.TabIndex = 69;
            this.label11.Text = "9.";
            // 
            // label53
            // 
            this.label53.BackColor = System.Drawing.Color.White;
            this.label53.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(243, 124);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(23, 19);
            this.label53.TabIndex = 70;
            this.label53.Text = "18.";
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.White;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(26, 242);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 18);
            this.label12.TabIndex = 68;
            this.label12.Text = "8.";
            // 
            // label52
            // 
            this.label52.BackColor = System.Drawing.Color.White;
            this.label52.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(243, 219);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(23, 19);
            this.label52.TabIndex = 66;
            this.label52.Text = "22.";
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.White;
            this.label13.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(26, 124);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 18);
            this.label13.TabIndex = 70;
            this.label13.Text = "3.";
            // 
            // label51
            // 
            this.label51.BackColor = System.Drawing.Color.White;
            this.label51.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(243, 100);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(23, 19);
            this.label51.TabIndex = 67;
            this.label51.Text = "17.";
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.White;
            this.label14.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(26, 219);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 18);
            this.label14.TabIndex = 66;
            this.label14.Text = "7.";
            // 
            // label50
            // 
            this.label50.BackColor = System.Drawing.Color.White;
            this.label50.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(243, 77);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(23, 19);
            this.label50.TabIndex = 56;
            this.label50.Text = "16.";
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.White;
            this.label15.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(26, 100);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 18);
            this.label15.TabIndex = 67;
            this.label15.Text = "2.";
            // 
            // label49
            // 
            this.label49.BackColor = System.Drawing.Color.White;
            this.label49.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(26, 77);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(19, 18);
            this.label49.TabIndex = 56;
            this.label49.Text = "1.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(72, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(323, 20);
            this.label1.TabIndex = 55;
            this.label1.Text = "* Decide Font You Want and Just Say Its Number: *";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.mainrichTextBox);
            this.panel1.Location = new System.Drawing.Point(47, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(604, 697);
            this.panel1.TabIndex = 0;
            // 
            // mainrichTextBox
            // 
            this.mainrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.mainrichTextBox.ContextMenuStrip = this.contextMenuStrip1;
            this.mainrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainrichTextBox.EnableAutoDragDrop = true;
            this.mainrichTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainrichTextBox.HideSelection = false;
            this.mainrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.mainrichTextBox.Name = "mainrichTextBox";
            this.mainrichTextBox.Size = new System.Drawing.Size(604, 697);
            this.mainrichTextBox.TabIndex = 3;
            this.mainrichTextBox.Text = "";
            this.mainrichTextBox.Click += new System.EventHandler(this.mainrichTextBox_Click);
            this.mainrichTextBox.TextChanged += new System.EventHandler(this.mainrichTextBox_TextChanged);
            this.mainrichTextBox.DoubleClick += new System.EventHandler(this.mainrichTextBox_DoubleClick);
            this.mainrichTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mainrichTextBox_KeyDown);
            this.mainrichTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.mainrichTextBox_KeyUp);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripSeparator1,
            this.toolStripButton5,
            this.toolStripButton6,
            this.toolStripSeparator2,
            this.UndotoolStripButton,
            this.RedotoolStripButton,
            this.toolStripButton9,
            this.toolStripSeparator3,
            this.cuttoolStripButton,
            this.copytoolStripButton,
            this.PastetoolStripButton,
            this.deltoolStripButton,
            this.toolStripSeparator5,
            this.BoldtoolStripButton,
            this.ItalictoolStripButton,
            this.UnderlinetoolStripButton,
            this.StrikethroughtoolStripButton,
            this.toolStripSeparator4,
            this.fontnametoolStripComboBox,
            this.fontsizetoolStripComboBox,
            this.toolStripSeparator6,
            this.toolStripButton15,
            this.toolStripButton14,
            this.toolStripButton16});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(695, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::latest_voicepad.Properties.Resources._1484225270_file_add;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "NewtoolStripButton";
            this.toolStripButton1.ToolTipText = "Creates New Document";
            this.toolStripButton1.Click += new System.EventHandler(this.NewtoolStripButton_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::latest_voicepad.Properties.Resources._1484224721_open_file;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "OpentoolStripButton";
            this.toolStripButton2.ToolTipText = "Opens New Document";
            this.toolStripButton2.Click += new System.EventHandler(this.OpentoolStripButton_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::latest_voicepad.Properties.Resources._1484224610_save;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "SavetoolStripButton";
            this.toolStripButton3.ToolTipText = "Save";
            this.toolStripButton3.Click += new System.EventHandler(this.SavetoolStripButton_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = global::latest_voicepad.Properties.Resources._1484224538_Save_as;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton4.Text = "SaveAstoolStripButton";
            this.toolStripButton4.ToolTipText = "Save As ";
            this.toolStripButton4.Click += new System.EventHandler(this.SaveAstoolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = global::latest_voicepad.Properties.Resources._1484223749_vector_66_15;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton5.Text = "PrinttoolStripButton";
            this.toolStripButton5.ToolTipText = "Print";
            this.toolStripButton5.Click += new System.EventHandler(this.PrinttoolStripButton_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = global::latest_voicepad.Properties.Resources._1484228290_document_print_preview;
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton6.Text = "PrintPreviewtoolStripButton";
            this.toolStripButton6.ToolTipText = "Print Preview";
            this.toolStripButton6.Click += new System.EventHandler(this.PrintPreviewtoolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // UndotoolStripButton
            // 
            this.UndotoolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.UndotoolStripButton.Enabled = false;
            this.UndotoolStripButton.Image = global::latest_voicepad.Properties.Resources._1484224300_icon_ios7_undo;
            this.UndotoolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.UndotoolStripButton.Name = "UndotoolStripButton";
            this.UndotoolStripButton.Size = new System.Drawing.Size(23, 22);
            this.UndotoolStripButton.Text = "UndotoolStripButton";
            this.UndotoolStripButton.ToolTipText = "Undo";
            this.UndotoolStripButton.Click += new System.EventHandler(this.UndotoolStripButton_Click);
            // 
            // RedotoolStripButton
            // 
            this.RedotoolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.RedotoolStripButton.Enabled = false;
            this.RedotoolStripButton.Image = global::latest_voicepad.Properties.Resources._1484224073_icon_ios7_redo;
            this.RedotoolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RedotoolStripButton.Name = "RedotoolStripButton";
            this.RedotoolStripButton.Size = new System.Drawing.Size(23, 22);
            this.RedotoolStripButton.Text = "RedotoolStripButton";
            this.RedotoolStripButton.ToolTipText = "Redo";
            this.RedotoolStripButton.Click += new System.EventHandler(this.RedotoolStripButton_Click);
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton9.Enabled = false;
            this.toolStripButton9.Image = global::latest_voicepad.Properties.Resources._1484228659_select_all;
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton9.Text = "SelectAlltoolStripButton";
            this.toolStripButton9.ToolTipText = "Select All";
            this.toolStripButton9.Click += new System.EventHandler(this.SelectAlltoolStripButton_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // cuttoolStripButton
            // 
            this.cuttoolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cuttoolStripButton.Enabled = false;
            this.cuttoolStripButton.Image = global::latest_voicepad.Properties.Resources._1484514405_Cut;
            this.cuttoolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cuttoolStripButton.Name = "cuttoolStripButton";
            this.cuttoolStripButton.Size = new System.Drawing.Size(23, 22);
            this.cuttoolStripButton.ToolTipText = "Cut";
            this.cuttoolStripButton.Click += new System.EventHandler(this.CuttoolStripButton_Click);
            // 
            // copytoolStripButton
            // 
            this.copytoolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copytoolStripButton.Enabled = false;
            this.copytoolStripButton.Image = global::latest_voicepad.Properties.Resources._1484514506_Copy;
            this.copytoolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copytoolStripButton.Name = "copytoolStripButton";
            this.copytoolStripButton.Size = new System.Drawing.Size(23, 22);
            this.copytoolStripButton.Text = "toolStripButton8";
            this.copytoolStripButton.ToolTipText = "Copy";
            this.copytoolStripButton.Click += new System.EventHandler(this.CopytoolStripButton_Click);
            // 
            // PastetoolStripButton
            // 
            this.PastetoolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PastetoolStripButton.Image = global::latest_voicepad.Properties.Resources._1484514537_Paste;
            this.PastetoolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PastetoolStripButton.Name = "PastetoolStripButton";
            this.PastetoolStripButton.Size = new System.Drawing.Size(23, 22);
            this.PastetoolStripButton.Text = "toolStripButton17";
            this.PastetoolStripButton.ToolTipText = "Paste";
            this.PastetoolStripButton.Click += new System.EventHandler(this.PastetoolStripButton_Click);
            // 
            // deltoolStripButton
            // 
            this.deltoolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deltoolStripButton.Enabled = false;
            this.deltoolStripButton.Image = global::latest_voicepad.Properties.Resources._1484518376_edit_delete;
            this.deltoolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deltoolStripButton.Name = "deltoolStripButton";
            this.deltoolStripButton.Size = new System.Drawing.Size(23, 22);
            this.deltoolStripButton.Text = "toolStripButton17";
            this.deltoolStripButton.ToolTipText = "Delete";
            this.deltoolStripButton.Click += new System.EventHandler(this.DeletetoolStripButton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // BoldtoolStripButton
            // 
            this.BoldtoolStripButton.CheckOnClick = true;
            this.BoldtoolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BoldtoolStripButton.Image = global::latest_voicepad.Properties.Resources.Bold_52px1;
            this.BoldtoolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BoldtoolStripButton.Name = "BoldtoolStripButton";
            this.BoldtoolStripButton.Size = new System.Drawing.Size(23, 22);
            this.BoldtoolStripButton.Text = "BoldtoolStripButton";
            this.BoldtoolStripButton.ToolTipText = "Bold";
            this.BoldtoolStripButton.Click += new System.EventHandler(this.BoldtoolStripButton_Click);
            // 
            // ItalictoolStripButton
            // 
            this.ItalictoolStripButton.CheckOnClick = true;
            this.ItalictoolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ItalictoolStripButton.Image = global::latest_voicepad.Properties.Resources.Italic_52px1;
            this.ItalictoolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ItalictoolStripButton.Name = "ItalictoolStripButton";
            this.ItalictoolStripButton.Size = new System.Drawing.Size(23, 22);
            this.ItalictoolStripButton.Text = "ItalictoolStripButton";
            this.ItalictoolStripButton.ToolTipText = "Italic";
            this.ItalictoolStripButton.Click += new System.EventHandler(this.ItalictoolStripButton_Click);
            // 
            // UnderlinetoolStripButton
            // 
            this.UnderlinetoolStripButton.CheckOnClick = true;
            this.UnderlinetoolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.UnderlinetoolStripButton.Image = global::latest_voicepad.Properties.Resources.Underline_52px1;
            this.UnderlinetoolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.UnderlinetoolStripButton.Name = "UnderlinetoolStripButton";
            this.UnderlinetoolStripButton.Size = new System.Drawing.Size(23, 22);
            this.UnderlinetoolStripButton.Text = "UnderLinetoolStripButton";
            this.UnderlinetoolStripButton.ToolTipText = "Underline";
            this.UnderlinetoolStripButton.Click += new System.EventHandler(this.UnderlinetoolStripButton_Click);
            // 
            // StrikethroughtoolStripButton
            // 
            this.StrikethroughtoolStripButton.CheckOnClick = true;
            this.StrikethroughtoolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.StrikethroughtoolStripButton.Image = global::latest_voicepad.Properties.Resources.Strikethrough_52px1;
            this.StrikethroughtoolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.StrikethroughtoolStripButton.Name = "StrikethroughtoolStripButton";
            this.StrikethroughtoolStripButton.Size = new System.Drawing.Size(23, 22);
            this.StrikethroughtoolStripButton.Text = "StrikeThroughtoolStripButton";
            this.StrikethroughtoolStripButton.ToolTipText = "Strikethrough";
            this.StrikethroughtoolStripButton.Click += new System.EventHandler(this.StrikethroughtoolStripButton_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // fontnametoolStripComboBox
            // 
            this.fontnametoolStripComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.fontnametoolStripComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fontnametoolStripComboBox.AutoSize = false;
            this.fontnametoolStripComboBox.BackColor = System.Drawing.SystemColors.Control;
            this.fontnametoolStripComboBox.DropDownHeight = 120;
            this.fontnametoolStripComboBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.fontnametoolStripComboBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fontnametoolStripComboBox.IntegralHeight = false;
            this.fontnametoolStripComboBox.Items.AddRange(new object[] {
            "Agency FB",
            "Algerian",
            "Arial",
            "Arial Rounded MT ",
            "Arial Unicode MS",
            "Baskerville Old Face",
            "Bauhaus 93",
            "Bell MT",
            "Berlin Sans FB",
            "Bernard MT Condensed",
            "Blackadder ITC",
            "Bodoni MT",
            "Bodoni MT Condensed",
            "Bodoni MT Poster Compressed",
            "Book Antiqua",
            "Bookman Old Style",
            "Bradley Hand ITC",
            "Broadway",
            "Brush Script MT ",
            "Calibri",
            "Californian FB",
            "Calisto MT",
            "Cambria & Cambria Math",
            "Candara",
            "Centaur",
            "Century",
            "Century Gothic",
            "Century Schoolbook",
            "Chiller",
            "Colonna MT",
            "Consolas",
            "Constantia",
            "Cooper Black",
            "Copperplate Gothic",
            "Corbel",
            "Curlz MT",
            "Edwardian Script ITC",
            "Elephant",
            "Engraveras",
            "Eras Bold ITC",
            "Eras Demi ITC",
            "Eras Light ITC",
            "Eras Medium ITC",
            "Felix Titling",
            "Footlight MT Light",
            "Forte",
            "Franklin",
            "Franklin Gothic",
            "Franklin Gothic Demi",
            "Franklin Gothic Demi Cond",
            "Franklin Gothic Heavy",
            "Freestyle Script",
            "French Script MT",
            "Garamond",
            "Gigi",
            "Gill Sans MT",
            "Gill Sans MT Condensed",
            "Gill Sans MT Ext Condensed",
            "Gill Sans Ultra ",
            "Gloucester MT Extra Condensed",
            "Gothic Book",
            "Goudy Old Style",
            "Goudy Stout",
            "Haettenschweiler",
            "Harlow Solid ",
            "High Tower Text",
            "Imprint MT Shadow",
            "Informal Roman",
            "Jokerman",
            "Juice ITC",
            "Kristen ITC",
            "Kunstler Script",
            "Lucida Bright ",
            "Lucida Bright Demibold",
            "Lucida Calligraphy ",
            "Lucida Fax",
            "Lucida Handwriting",
            "Lucida Sans ",
            "Lucida Sans Demibold Roman",
            "Lucida Sans Typewriter ",
            "Magneto",
            "Maiandra GD",
            "Matura MT Script Capitals",
            "Microsoft Sans Serif",
            "Mistral",
            "Modern No. 20",
            "MS Mincho",
            "MS Outlook",
            "MS Reference Sans Serif",
            "MS Reference Specialty",
            "MT Extra",
            "Niagara Engraved",
            "Niagara Solid",
            "OCR A Extended",
            "Old English Text MT",
            "Onyx",
            "Palatino Linotype",
            "Parchment",
            "Perpetua",
            "Playbill",
            "Poor Richard",
            "Pristina",
            "Rage ",
            "Ravie Rockwell",
            "Rockwell ",
            "Rockwell Condensed",
            "Segoe UI*",
            "Showcard Gothic",
            "Snap ITC",
            "Stencil",
            "Tempus Sans ITC",
            "Tw Cen MT",
            "Tw Cen MT Condensed",
            "Viner Hand ITC",
            "Vladimir Script",
            "Wide Latin",
            "Wingdings 2",
            "Wingdings 3"});
            this.fontnametoolStripComboBox.MaxDropDownItems = 10;
            this.fontnametoolStripComboBox.MaxLength = 10;
            this.fontnametoolStripComboBox.Name = "fontnametoolStripComboBox";
            this.fontnametoolStripComboBox.Size = new System.Drawing.Size(140, 23);
            this.fontnametoolStripComboBox.Sorted = true;
            this.fontnametoolStripComboBox.Text = "Microsoft Sans Serif";
            this.fontnametoolStripComboBox.ToolTipText = "Change Text Font";
            this.fontnametoolStripComboBox.TextUpdate += new System.EventHandler(this.fontnametoolStripComboBox_TextUpdate);
            this.fontnametoolStripComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fontnametoolStripComboBox_KeyPress);
            this.fontnametoolStripComboBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.fontnametoolStripComboBox_KeyUp);
            this.fontnametoolStripComboBox.TextChanged += new System.EventHandler(this.fontnametoolStripComboBox_TextChanged);
            // 
            // fontsizetoolStripComboBox
            // 
            this.fontsizetoolStripComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.fontsizetoolStripComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fontsizetoolStripComboBox.AutoSize = false;
            this.fontsizetoolStripComboBox.BackColor = System.Drawing.SystemColors.Menu;
            this.fontsizetoolStripComboBox.DropDownHeight = 120;
            this.fontsizetoolStripComboBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.fontsizetoolStripComboBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fontsizetoolStripComboBox.IntegralHeight = false;
            this.fontsizetoolStripComboBox.Items.AddRange(new object[] {
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59",
            "60",
            "61",
            "62",
            "63",
            "64",
            "65",
            "66",
            "67",
            "68",
            "69",
            "70",
            "71",
            "72"});
            this.fontsizetoolStripComboBox.MaxDropDownItems = 10;
            this.fontsizetoolStripComboBox.MaxLength = 10;
            this.fontsizetoolStripComboBox.Name = "fontsizetoolStripComboBox";
            this.fontsizetoolStripComboBox.Size = new System.Drawing.Size(43, 23);
            this.fontsizetoolStripComboBox.Text = "12";
            this.fontsizetoolStripComboBox.ToolTipText = "Font Size";
            this.fontsizetoolStripComboBox.TextUpdate += new System.EventHandler(this.fontsizetoolStripComboBox_TextUpdate);
            this.fontsizetoolStripComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fontsizetoolStripComboBox_KeyDown);
            this.fontsizetoolStripComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fontsizetoolStripComboBox_KeyPress);
            this.fontsizetoolStripComboBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.fontsizetoolStripComboBox_KeyUp);
            this.fontsizetoolStripComboBox.TextChanged += new System.EventHandler(this.fontsizetoolStripComboBox_TextChanged);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton15
            // 
            this.toolStripButton15.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton15.Image = global::latest_voicepad.Properties.Resources.Oxygen_Icons_org_Oxygen_Actions_format_stroke_color;
            this.toolStripButton15.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton15.Name = "toolStripButton15";
            this.toolStripButton15.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton15.Text = "TextColortoolStripButton";
            this.toolStripButton15.ToolTipText = "Change Text Color";
            this.toolStripButton15.Click += new System.EventHandler(this.TextColortoolStripButton_Click);
            // 
            // toolStripButton14
            // 
            this.toolStripButton14.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton14.Image = global::latest_voicepad.Properties.Resources._1484242285_preferences_desktop_font;
            this.toolStripButton14.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton14.Name = "toolStripButton14";
            this.toolStripButton14.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton14.Text = "FormatFonttoolStripButton";
            this.toolStripButton14.ToolTipText = "Text Format";
            this.toolStripButton14.Click += new System.EventHandler(this.FormatTexttoolStripButton_Click);
            // 
            // toolStripButton16
            // 
            this.toolStripButton16.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton16.Image = global::latest_voicepad.Properties.Resources._1484240754_preferences_desktop_color;
            this.toolStripButton16.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton16.Name = "toolStripButton16";
            this.toolStripButton16.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton16.Text = "PageColortoolStripButton";
            this.toolStripButton16.ToolTipText = "Change Page Color";
            this.toolStripButton16.Click += new System.EventHandler(this.PageColortoolStripButton_Click);
            // 
            // VoicepadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 776);
            this.Controls.Add(this.toolStripContainer1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "VoicepadForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Untitled - Voicepad";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VoicepadForm_FormClosing);
            this.Load += new System.EventHandler(this.VoicepadForm_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.colorpanel.ResumeLayout(false);
            this.colorpanel.PerformLayout();
            this.fontpanel.ResumeLayout(false);
            this.fontpanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printPreviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dateTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formatFontToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeTextColorToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem normalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem boldtoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem italicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem underlineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem strikethroughToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutVoicepadToolStripMenuItem;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton UndotoolStripButton;
        private System.Windows.Forms.ToolStripButton RedotoolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Windows.Forms.ToolStripButton BoldtoolStripButton;
        private System.Windows.Forms.ToolStripButton ItalictoolStripButton;
        private System.Windows.Forms.ToolStripButton UnderlinetoolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton StrikethroughtoolStripButton;
        private System.Windows.Forms.ToolStripButton toolStripButton14;
        private System.Windows.Forms.ToolStripButton toolStripButton15;
        private System.Windows.Forms.ToolStripButton toolStripButton16;
        private System.Windows.Forms.ToolStripStatusLabel MessagetoolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel CapstoolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripButton cuttoolStripButton;
        private System.Windows.Forms.ToolStripButton copytoolStripButton;
        private System.Windows.Forms.ToolStripButton PastetoolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripButton deltoolStripButton;
        private System.Windows.Forms.ToolStripMenuItem voiceControlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem voiceCommandsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePageColorToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox fontsizetoolStripComboBox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripComboBox fontnametoolStripComboBox;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        public System.Windows.Forms.ToolStripMenuItem enabledToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem disabledToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel fontpanel;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        public System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel colorpanel;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.Button applybtn;
        private System.Windows.Forms.Button cancelbtn;
        private System.Windows.Forms.Label greylabel;
        private System.Windows.Forms.Label purplelabel;
        private System.Windows.Forms.Label bluelabel;
        private System.Windows.Forms.Label indigolabel;
        private System.Windows.Forms.Label brownlabel;
        private System.Windows.Forms.Label orangelabel;
        private System.Windows.Forms.Label pinklabel;
        private System.Windows.Forms.Label yellowlabel;
        private System.Windows.Forms.Label maroonlabel;
        private System.Windows.Forms.Label lightgreenlabel;
        private System.Windows.Forms.Label greenlabel;
        private System.Windows.Forms.Label redlabel;
        private System.Windows.Forms.Label lightbluelabel;
        private System.Windows.Forms.Label whitelabel;
        private System.Windows.Forms.Label blacklabel;
        private System.Windows.Forms.RichTextBox mainrichTextBox;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
    }
}

